import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(27, 32, 35)',
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    header: {
        color: 'rgb(20,120, 169 )',
        paddingTop: 50,
        paddingBottom: 40,
        paddingLeft: 40,
        fontSize: 25,
        fontWeight: 'bold'
    },
    info: {
        backgroundColor: 'rgb(27, 32, 35)',
    },
    row: {
        paddingLeft: 40,
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        paddingBottom: 20,
        paddingTop: 20
    },
    title: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold'
    },
    content: {
        color: 'gray',
        fontSize: 10
    },
    coloredTitle: {
       color: 'rgb(20,120, 169 )',
        fontSize: 16,
        fontWeight: 'bold'

    },
    catRow: {
        flexDirection: 'row',
        backgroundColor: 'rgb(27, 32, 35)',
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
    },
    catItem: {
        width: '33%',
        padding: 10,
        borderRightWidth: 1,
        borderRightColor: 'gray',
        paddingTop: 25,
        paddingBottom: 25,
    },
    lastCatItem: {
        width: '33%',
        padding: 10,
        paddingTop: 25,
        paddingBottom: 25,
    },
    footer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 25,
        paddingBottom: 25
    },
    topFooter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logoContainer: {
        flexDirection: 'row'
    },
    image: {
        width: 27,
        resizeMode: 'contain',
        marginRight: 8
    },
    logoLeftText: {
        color: 'gray',
        fontSize: 11
    },
    logoRightText: {
        color: 'rgb(185, 161, 25)',
        fontSize: 14,
        fontWeight: 'bold'
    },
    logoBottomText: {
        color: 'gray',
        fontSize: 13
    },
    footerLable: {
        color: 'white',
        fontSize: 10,
        paddingBottom: 5

    },
    footerSubLabel: {
        fontSize: 10,
        color: 'rgb(20,120, 169 )'
    }

});
