
import React, {Component} from 'react';
import {Text, View, ImageBackground,ScrollView, Image} from 'react-native';
import styles from './styles'
import SidebarCategory from "../components/sidebarCategory/index";
import image from '../assets/big-gold-official.png'
export default class Sidebar extends React.Component {
    constructor(){
        super();
        this.state = {

        };
    }
    render(){
        return (
            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.header}>Take a look around...</Text>
                    <View style={styles.info}>
                        <View style={styles.row}>
                            <Text style={styles.title}>Calls for Tenders (Bids)</Text>
                            <Text style={styles.content}>About his childhood, Danial becomes suspicious</Text>

                        </View>
                        <View style={styles.row}>
                            <Text style={styles.coloredTitle}>Aucitions</Text>
                            <Text style={styles.content}>About his childhood, Danial</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.title}>Psychology Tests</Text>
                            <Text style={styles.content}>About his childhood, Danial becomes</Text>
                        </View>
                    </View>
                    <View style={styles.catRow}>
                        <View style={styles.catItem}>
                            <SidebarCategory icon="briefcase-24" label="Job Offers"/>
                        </View>
                        <View style={styles.catItem}>
                            <SidebarCategory icon="building" label="Companies"/>
                        </View>
                        <View style={styles.lastCatItem}>
                            <SidebarCategory icon="wand-11" label="Influencers"/>
                        </View>
                    </View>
                    <View style={styles.catRow}>
                        <View style={styles.catItem}>
                            <SidebarCategory icon="shirt-business" label="Departments"/>
                        </View>
                        <View style={styles.catItem}>
                            <SidebarCategory icon="multiple-11" label="Groups"/>
                        </View>
                        <View style={styles.lastCatItem}>
                            <SidebarCategory icon="alert-square-que" label="Help Center"/>
                        </View>
                    </View>
                    <View style={styles.footer}>
                        <View style={styles.topFooter}>
                            <Image style={styles.image} source={image} />
                            <View>
                                <View style={styles.logoContainer}>
                                    <Text style={styles.logoLeftText}>Upgrade to </Text>
                                    <Text style={styles.logoRightText}>PREMIUM</Text>
                                </View>
                                <Text style={styles.logoBottomText}>Use All The Features</Text>
                            </View>
                        </View>
                        <Text style={styles.footerLable}>Unlock The Power of Anjoman App</Text>
                        <Text style={styles.footerSubLabel}>[Read more about advantage]</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

