import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';

import styles from './styles';

const Loader = (props) => {
    return (
        <View style={styles.container}>
            <ActivityIndicator
                size={props.size}
                animating={props.animating}
                color={props.color}
                {...props}
            />
            <Text style={styles.text}>{props.send ? 'Loading' : 'Loading'}</Text>
        </View>
    );
};
Loader.defaultProps = {
    size: 'large',
    animating: true,
    color: 'rgb(20, 122, 170)',
    send:true
};
export default Loader;

