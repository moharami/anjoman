import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30,

    },
    text: {
        textAlign: 'center',
        fontSize:13,
        color: "gray",
        paddingTop: 20
    }
});