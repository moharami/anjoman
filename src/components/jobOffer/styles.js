import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        backgroundColor: 'white'
    },
    header: {
        paddingTop: 10,
        paddingRight: 20,
        paddingLeft: 20,
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    },
    title: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold'
    },
    content: {
        padding: 10,
        paddingRight: 15,
        paddingLeft: 20,

    },
    contentText: {
        fontSize: 12,
        color: 'black',
        lineHeight: 20
    },
    footer: {
        borderTopWidth: 1,
        borderTopColor: 'lightgray',
    },
    image: {
        width: 75,
        height: 30,
        resizeMode: 'contain'
    },
    top: {
        padding: 5,
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
    },
    right: {
        paddingLeft: 15,
        width: '50%',
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
    },
    left: {
        // paddingLeft: 20,
        width: '50%',
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
    },
    label: {
        fontSize: 10,
        color: 'rgb(20, 122, 170)',
        paddingRight: 7,
        fontWeight: 'bold'
    },
    value: {
        fontSize: 10,
        color: 'black',
        paddingRight: 7
    },
    detailsText: {
        color: 'white',
        fontSize: 14
    },
    applyText: {
        color: 'white',
        fontSize: 14
    },
    bottom: {
        backgroundColor: 'rgb(0, 113, 173)',
        flexDirection:'row',
        // justifyContent: "space-between",
        // alignItems: "center",
    },
    detail: {
        width: '50%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 10,
        borderRightColor: 'white',
        borderRightWidth: 1
    },
    apply: {
        width: '50%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 10

    }



});
