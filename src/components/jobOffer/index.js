import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import image from '../../assets/eskanto.png'
import styles from './styles'
import Nucleo from '../nucleo'

class JobOffer extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.title}>UX Designer</Text>
                    <Image style={styles.image} source={image} />
                </View>
                <View style={styles.content}>
                    <Text style={styles.contentText}>
                        something that is contained:the contents of a box.contained:the contents contained:the contents fsomething that is contained:
                    </Text>
                </View>
                <View style={styles.footer}>
                    <View style={styles.top}>
                        <View style={styles.right}>
                            <Text style={styles.label}>Location</Text>
                            <Text style={styles.value}>Tehran</Text>
                        </View>
                        <View style={styles.left}>
                            <Text style={styles.label}>Salary Range</Text>
                            <Text style={styles.value}>75 MS PY</Text>
                        </View>
                    </View>
                    <View style={styles.bottom}>
                        <View style={styles.detail}>
                            <Nucleo style={{color: '#fff', fontSize: 14, paddingRight: 5}} name="segmentation"/>
                            <Text style={styles.detailsText}>Details</Text>
                        </View>
                        <View style={styles.apply}>
                            <Nucleo style={{color: '#fff', fontSize: 14, paddingRight: 5}} name="send"/>
                            <Text style={styles.applyText}>Apply</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
export default JobOffer