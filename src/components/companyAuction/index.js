import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles'
import blueOfficial from '../../assets/blue-official.png'
import Nucleo from '../nucleo'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class Auction extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.left}>
                        <View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>Amir Bageri</Text>
                                <Image source={blueOfficial} />
                            </View>
                            <Text style={styles.content}>Amir bageri  Usually, contents.
                                something
                            </Text>
                        </View>
                    </View>
                    <View style={styles.right}>
                        <View style={styles.details}>
                            <Icon name="bars" size={12} color="rgb(50, 152, 200)" />

                            <Text style={styles.detailsText}>Details</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
export default Auction;