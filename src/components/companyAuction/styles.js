import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    header: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "flex-start",
    },
    iconText: {
        color: 'gray',
        fontSize: 10,
        textAlign: 'right'
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '70%'
    },
    image:{
        borderRadius: 90,
        width: 55,
        height: 55,
        marginRight: 10,
        borderWidth: 2,
        borderColor: 'white'
    },
    content: {
        fontSize: 12,
        color: 'black'
    },
    title: {
        fontSize: 12,
        fontWeight: 'bold',
        color: 'black',
        paddingRight: 3
    },
    titleContainer: {
        flexDirection:'row',

    },
    details: {
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(245, 245, 245)',
        borderRadius: 3,
        marginRight: 4
    },
    detailsText: {
        color: 'rgb(50, 152, 200)',
        fontSize: 12,
        paddingLeft: 8
    },
    right: {
        width: '27%',
    }
});
