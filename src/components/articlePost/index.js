import React, {Component} from 'react';
import {View, Text, ScrollView, TouchableOpacity, TextInput,AsyncStorage, Image} from 'react-native';
import styles from './styles'
import Nucleo from '../nucleo'
import Textarea from 'react-native-textarea';
import Axios from 'axios'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import bg from '../../assets/Logo.png';
import Loader from '../../components/loader'
import Ripple from 'react-native-material-ripple';
import Items from '../../components/footerItems'
import {Actions} from 'react-native-router-flux'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://professionall.azarinpro.info/api';
Axios.defaults.baseURL = url;

class ArticlePost extends Component {
    constructor(props){
        super(props);
        this.state = {
            content: '',
            title: '',
            ImageSource: null,
            userId: null,
            loading: false
        };
    }
    // async getId(key){
    //     try {
    //         await AsyncStorage.getItem(key).then((info) => {
    //             const newInfo = JSON.parse(info);
    //             if(newInfo.token !== null) {
    //                 console.log('forrrr useriiiiid',newInfo.token);
    //                 Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token ;
    //                 Axios.get('/getUser').then(response=> {
    //                     store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data});
    //                     this.setState({userId: response.data.id})
    //                 })
    //                 .catch((error)=> {
    //                     alert('error ocured');
    //                     console.log(error.response);
    //                     }
    //                 );
    //             }
    //         });
    //
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }
    // componentWillMount(){
    //     this.getId('token');
    // }
    // articlePost(){
    //     this.setState({loading: true})
    //     Axios.post('/post', {
    //         postable_type: "App\\User",
    //         postable_id: this.state.userId,
    //         title: this.state.title,
    //         content: this.state.content,
    //         type: "article"
    //     }).then(response=> {
    //         this.setState({loading: false});
    //         alert('your post added successfully');
    //         console.log(response);
    //     }).catch( (error)=> {
    //             this.setState({loading: false})
    //             console.log(error.response);
    //             alert('some problem ocured');
    //         }
    //     );
    // }
    // clearPost(){
    //     this.setState({title: '', content: ''})
    // }
    // othetPost(num){
    //     if(num === 1){
    //         Actions.push('PhotoPost');
    //     }
    //     else{
    //         Actions.push('VideoPost');
    //     }
    // }
    render() {
        return (
            <View style={styles.master}>
                {/*<View style={styles.header}>*/}
                    {/*<Text style={{color: 'white', fontSize: 18, paddingLeft: 20, fontWeight: 'bold'}}>New Post</Text>*/}
                    {/*<Image style={{width: 90, height: 20, resizeMode: 'contain', marginRight: 10}} source={bg} />*/}
                {/*</View>*/}
                {/*<View style={styles.postType}>*/}
                    {/*<Ripple style={{ width: '34%',flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(27, 32, 35)'}}>*/}
                        {/*<Nucleo style={{color: 'rgb(19, 122, 167)', fontSize: 12, paddingRight: 4}} name='edit-74' />*/}
                        {/*<Text style={{color: 'rgb(19, 122, 167)', fontSize: 12 }}>Write an Article</Text>*/}
                    {/*</Ripple>*/}
                    {/*<Ripple style={{width: '33%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(41, 49, 52)'}} onPress={()=> this.othetPost(1)}>*/}
                        {/*<Nucleo style={{color: 'rgb(75, 80, 84)', fontSize: 12, paddingRight: 4}} name='picture' />*/}
                        {/*<Text style={{color: 'rgb(75, 80, 84)', fontSize: 12}}>Photo / Album</Text>*/}
                    {/*</Ripple>*/}
                    {/*<Ripple style={{width: '33%', flexDirection: 'row' ,alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(41, 49, 52)'}} onPress={()=> this.othetPost(2)}>*/}
                        {/*<Nucleo style={{color: 'rgb(75, 80, 84)', fontSize: 12, paddingRight: 4}} name='action-74' />*/}
                        {/*<Text style={{color: 'rgb(75, 80, 84)', fontSize: 12}}>Video</Text>*/}
                    {/*</Ripple>*/}
                {/*</View>*/}
                {/*<ScrollView style={styles.scroll}>*/}
                    {/*{*/}
                        {/*this.state.loading ? <Loader send={false} /> : (*/}
                    {/*<View style={styles.container}>*/}
                        {/*<Text style={styles.title}>Title</Text>*/}
                        {/*<TextInput*/}
                            {/*placeholder="title"*/}
                            {/*underlineColorAndroid='transparent'*/}
                            {/*value={this.state.title}*/}
                            {/*style={{ height: 32,  backgroundColor: 'rgb(241, 241, 241)', paddingLeft: 5, paddingTop: 8, width: '100%' }}*/}
                            {/*onChangeText={(title) => this.setState({title})} />*/}
                        {/*<Text style={styles.article}>Article</Text>*/}
                        {/*<View style={styles.articleContainer}>*/}
                        {/*<Textarea*/}
                            {/*containerStyle={styles.textareaContainer}*/}
                            {/*style={styles.textarea}*/}
                            {/*defaultValue={this.state.content}*/}
                            {/*maxLength={120}*/}
                            {/*placeholderTextColor={'#c7c7c7'}*/}
                            {/*underlineColorAndroid={'transparent'}*/}
                            {/*value={this.state.content}*/}
                            {/*onChangeText={(content) => this.setState({content})}*/}
                        {/*/>*/}
                        {/*</View>*/}
                        {/*<View style={styles.footer}>*/}
                            {/*<TouchableOpacity onPress={() => this.clearPost()}>*/}
                                {/*<View style={styles.clearContainer}>*/}
                                    {/*<Text style={styles.clear} >Clear Text</Text>*/}
                                {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                            {/*<TouchableOpacity onPress={() => this.articlePost()}>*/}
                                {/*<View style={styles.postButton}>*/}
                                    {/*<Nucleo style={{color: '#fff', fontSize: 14, paddingLeft: 5, paddingTop: 2}} name='send' />*/}
                                    {/*<Text style={styles.postText}>Post to Timeline</Text>*/}
                                {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                        {/*)*/}
                    {/*}*/}
                {/*</ScrollView>*/}
                <Items active={false}/>
            </View>
        );
    }
}
export default ArticlePost;
