import React, {Component} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import styles from './styles'
import image from '../../assets/article.jpg'
import Nucleo from '../nucleo'

class PortfolioItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={image} />
                <Text style={styles.caption}>Gallery Name Comes Here</Text>
                <Text style={styles.date}>July 2018</Text>
                <Text style={styles.location}>Tehran-Iran</Text>
            </View>
        );
    }
}
export default PortfolioItem;