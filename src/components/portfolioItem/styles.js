import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        marginRight: 35,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: '87%',
        height: 120,
        flex:1,
        resizeMode:'contain',
        overflow: 'hidden'
    },
    caption: {
        fontSize: 15,
        color: 'rgb(107, 104, 104)',
        fontWeight: 'bold',
        paddingTop: 8
    },
    date: {
        fontSize: 13,
        color: 'rgb(150, 150, 150)',
        fontWeight: 'bold',
        paddingTop: 5
    },
    location: {
        fontSize: 13,
        color: 'rgb(150, 150, 150)',
        fontWeight: 'bold',
    }
});
