import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Nucleo from '../nucleo'
import styles from './styles'

class SidebarCategory extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={styles.container}>
                <Nucleo style={{color: 'rgb(20,120, 169 )', fontSize: 20}} name={this.props.icon}/>
                <Text style={styles.label}>{this.props.label}</Text>
            </View>
        );
    }
}
export default SidebarCategory;