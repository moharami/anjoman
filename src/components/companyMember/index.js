import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles'
import image from '../../assets/user.png'
import blueOfficial from '../../assets/blue-official.png'
import Nucleo from '../nucleo'

class Member extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.left}>
                        <Image style={styles.image} source={image} />
                        <View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>Amir Bageri</Text>
                                <Image source={blueOfficial} />
                            </View>
                            <Text style={styles.content}>Amir bageri  Usually, contents.
                                something
                            </Text>
                        </View>
                    </View>

                </View>
            </View>
        );
    }
}
export default Member;