import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        paddingTop: 5,
        paddingBottom: 5,
    },
    header: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "flex-start",
    },
    iconText: {
        color: 'gray',
        fontSize: 10,
        textAlign: 'right'
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '80%'
    },
    image:{
        borderRadius: 90,
        width: 55,
        height: 55,
        marginRight: 10,
        borderWidth: 2,
        borderColor: 'white'
    },
    content: {
        fontSize: 12,
        color: 'black'
    },
    title: {
        fontSize: 12,
        fontWeight: 'bold',
        color: 'black',
        paddingRight: 3
    },
    titleContainer: {
        flexDirection:'row',

    }
});
