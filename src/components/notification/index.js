import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import image from '../../assets/user.png'
import Nucleo from '../nucleo'

import styles from './styles'

class Notification extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
        <View style={styles.container}>
            <View style={{backgroundColor:'#158abf',position:'absolute',top:-10,right:-10,width:20,height:20,transform:[{rotate:'45deg'}]}}/>
            <View style={styles.header}>
                <View style={styles.left}>
                    <Image style={styles.image} source={image} />
                    <Text style={styles.content}>Amir bageri  Usually, contents.
                        something that is contained:
                        the contents of a box.
                    </Text>
                </View>
                <View style={styles.right}>
                    <Text style={styles.iconText}>14H</Text>
                    <Nucleo style={{color: 'rgb(52, 117, 183)', fontSize: 14}} name="menu-right"/>
                </View>
            </View>
        </View>
        );
    }
}
export default Notification;