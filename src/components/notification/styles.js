
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray'
    },
    header: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    },
    right: {
        justifyContent: "flex-end",
        alignItems: "center",
    },
    iconText: {
        color: 'gray',
        fontSize: 10,
        textAlign: 'right'
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '70%'
    },
    image:{
        borderRadius: 90,
        width: 30,
        height: 30,
        marginRight: 10
    },
    content: {
        fontSize: 10,
    },
});
