import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {ParallaxImage} from 'react-native-snap-carousel';
import styles from './styles/SliderEntry.style';

export default class SliderEntry extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };

    get image() {
        const {data: {illustration}, parallax, parallaxProps, even} = this.props;

        return parallax ? (
            <ParallaxImage
                resizeMode="cover"
                source={{uri: illustration}}
                containerStyle={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
                style={styles.image}
                parallaxFactor={0.35}
                showSpinner={true}
                spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
                {...parallaxProps}
            />
        ) : (
            <Image
                source={{uri: illustration}}
                style={styles.image}
            />
        );
    }

    render() {
        const {data: {title}, even} = this.props;


        return (
            <TouchableOpacity
                activeOpacity={1}
                style={styles.slideInnerContainer}
                onPress={() => {
                    console.log(`You've clicked '${title}'`);
                }}
            >
                <View style={[styles.imageContainer, even ? styles.imageContainerEven : {}]}>
                    {this.image}
                        <View style={{
                            position: 'absolute',
                            bottom: 0,
                            right: 0,
                            left: 0,
                            // paddingHorizontal: 30,
                            paddingVertical: 10,

                        }}>
                            <Text style={{color: '#fff',textAlign:'right'}}>{title}</Text>
                        </View>

                </View>

            </TouchableOpacity>
        );
    }
}
