import React from 'react';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from './styles/SliderEntry.style';
import SliderItem from './item';
import styles from './styles/index.style';

const  renderItemWithParallax = ({item, index}, parallaxProps) =>{
    return (
        <SliderItem
            data={item}
            even={(index + 1) % 2 === 0}
            parallax={true}
            parallaxProps={parallaxProps}
        />
    );
}
export default  SlideShow = (props) => {
        return (
            <Carousel
                data={props.items}
                renderItem={renderItemWithParallax}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                hasParallaxImages={true}
                inactiveSlideScale={0.94}
                inactiveSlideOpacity={0.7}
                enableMomentum={false}
                containerCustomStyle={styles.slider}
                loop={true}
                loopClonesPerSide={2}
                autoplay={true}
                autoplayDelay={500}
                autoplayInterval={3000}
            />
        );
}
