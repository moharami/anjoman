import React, {Component} from 'react';
import {View, Text,StyleSheet} from 'react-native';
import Nucleo from '../nucleo'
import {store} from '../../config/store';
import Ripple from "react-native-material-ripple";
import {Actions} from 'react-native-router-flux'

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
});

class MenuButtonLeft extends Component {
    countCounter = () => {
        return 0
    };

    render() {
        const {navigate} = this.props;
        return (
            <Ripple
                onPress={() => {
                    store.dispatch({type: 'SEARCH_PAGE_ENABLED'});
                    Actions.push('Search')
                }

                }
            >
                <View style={styles.container}>
                    <Nucleo name={"zoom-2"} size={17} color="#fff" />
                </View>
            </Ripple>
        );
    }
}


export default MenuButtonLeft;