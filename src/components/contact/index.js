import React, {Component} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import styles from './styles'
import image from '../../assets/user.png'
import Nucleo from '../nucleo'

class Contact extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{backgroundColor:'#158abf',position:'absolute',top:-10,right:-10,width:20,height:20,transform:[{rotate:'45deg'}]}}/>
                <View style={styles.header}>
                    <View style={styles.left}>
                        <Image style={styles.image} source={image} />
                        <View>
                            <Text style={styles.title}>Amir Bageri</Text>
                            <Text style={styles.content}>Amir bageri  Usually, contents.
                                something that is contained:

                            </Text>
                        </View>
                    </View>
                    <View style={styles.right}>
                        <Nucleo style={{color: 'rgb(52, 117, 183)', fontSize: 12}} name="email-85"/>
                        <Nucleo style={{color: 'red', fontSize: 11, paddingLeft: 13}} name="trash-simple"/>
                    </View>
                </View>
            </View>
        );
    }
}
export default Contact;