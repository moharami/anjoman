import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        padding: 12,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray',
        backgroundColor: 'white'
    },
    header: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    },

    iconText: {
        color: 'gray',
        fontSize: 10,
        textAlign: 'right'
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '70%'
    },
    right:{
        flexDirection:'row',
    },
    image:{
        borderRadius: 90,
        width: 40,
        height: 40,
        marginRight: 10,
        borderWidth: 2,
        borderColor: 'white'
    },
    content: {
        fontSize: 10,
        color: 'black'
    },
    title: {
        fontSize: 12,
        color: 'black'

    },
    date: {
        fontSize: 8,
        color: '#158abf'
    }
});
