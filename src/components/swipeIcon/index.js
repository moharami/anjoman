import React, {Component} from 'react';
import {View, Text} from 'react-native';

import styles from './styles'
import Nucleo from '../nucleo'

class SwipeIcon extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={{flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: this.props.backgroundColor,
                width: '21%',
            }}>
                <Nucleo style={{color: '#fff', fontSize: 16}} name={this.props.icon}/>
                <Text style={styles.label}>{this.props.label}</Text>
            </View>
        );
    }
}
export default SwipeIcon;