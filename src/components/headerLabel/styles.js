
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: 'center',
        flexDirection: 'row'
    },
    logoContainer: {
        height: 15,
        width: 15,
        borderRadius: 20,
        backgroundColor: "red",
        justifyContent: "center",
        alignItems: "center"

    },
    logoText: {
        backgroundColor: "transparent",
        fontWeight: "bold",
        fontSize: 12,
        color: "white",
    },
    label: {
        color: 'white',
        paddingLeft: 7,
        fontSize: 16

    }
});
