import React, {Component} from 'react';
import {View, Text} from 'react-native';

import styles from './styles'

class HeaderLabel extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={styles.container}>
                {this.props.notification ?  <View style={styles.logoContainer}>
                    <Text style={styles.logoText}>9</Text>
                </View>: null }

                <Text style={styles.label}>{this.props.label}</Text>
            </View>
        );
    }
}
export default HeaderLabel;