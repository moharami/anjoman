
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        padding: 10,
        backgroundColor: 'white'
    },
    body: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '90%'
    },
    image:{
        width: 60,
        height: 40,
        marginRight: 13,
        // marginLeft: 20
    },
    content: {
        fontSize: 12,
        color: 'black',
        lineHeight: 20
    },
});
