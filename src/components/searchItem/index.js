import React, {Component} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import styles from './styles'
import image from '../../assets/article.png'
import Nucleo from '../nucleo'

class SearchItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.left}>
                        <Image style={styles.image} source={image} />
                        <Text style={styles.content}>Amir bageri  Usually, contents.
                            something that is contained:
                            the contents of a box.
                            {/*{this.props.substr([0, 70]) +'...'}*/}

                        </Text>
                    </View>

                </View>
            </View>
        );
    }
}
export default SearchItem;