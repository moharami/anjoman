import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingBottom: 50,
    },
    header: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    },
    iconText: {
        color: 'gray',
        fontSize: 10,
        textAlign: 'right'
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '68%'
    },
    image:{
        width: 10,
        resizeMode: 'contain',
        marginRight: 10
    },
    content: {
        fontSize: 12,
        color: 'black'
    },
    title: {
        fontSize: 12,
        fontWeight: 'bold',
        color: 'black',
        paddingRight: 3
    },
    titleContainer: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",

    },
    follow: {
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(245, 245, 245)',
        borderRadius: 3,
        marginRight: 4,
        paddingRight: 15
    },
    following: {
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(50, 152, 200)',
        borderRadius: 3,
        marginRight: 4
    },
    detailsText: {
        color: 'rgb(50, 152, 200)',
        fontSize: 12,
        paddingLeft: 8
    },
    right: {
        width: '30%',
    },
    followText: {
        fontStyle: 'italic',
        fontSize: 9,
        paddingTop: 5
    },
    followingText: {
        fontSize: 14,
        color: 'white'
    },
    followButtonText: {
        fontSize: 14,
        color: 'rgb(50, 152, 200)',
    }

});
