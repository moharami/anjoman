import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles'
import blueOfficial from '../../assets/blue-official.png'
import Nucleo from '../nucleo'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import image from '../../assets/ex1.png'
import user from '../../assets/user.png'

class RelatedCompanyItems extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.left}>
                        <Image source={this.props.follow ? image: user} style={{width: 50, height: 50, borderRadius: this.props.circle? 30: 3, marginRight: 10}}/>
                        <View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title}>Amir Bageri</Text>
                                <Image source={blueOfficial} style={styles.image} />
                            </View>
                            <Text style={styles.content}>Amir bageri  Usually, contents
                                {/*{this.props.substr([0, 30]) +'...'}*/}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.right}>
                        {
                            this.props.follow ? <View style={styles.follow}>
                                <Nucleo style={{color: 'rgb(50, 152, 200)', fontSize: 11, paddingRight: 8}} name="single-01"/>
                                <Text style={styles.followButtonText}>Follow</Text>
                            </View>:
                                <View style={styles.following}>
                                    <Nucleo style={{color: '#fff', fontSize: 11, paddingRight: 8}} name="single-01"/>
                                    <Text style={styles.followingText}>Following</Text>
                                </View>
                        }
                        <Text style={styles.followText}>Followed by 8.6M others</Text>
                    </View>
                </View>
            </View>
        );
    }
}
export default RelatedCompanyItems;