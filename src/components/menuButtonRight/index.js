import React, {Component} from 'react';
import {View, Text,StyleSheet} from 'react-native';
import Ripple from "react-native-material-ripple";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Nucleo from '../nucleo'
import {Actions} from 'react-native-router-flux'
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
});

class MenuButtonRight extends Component {
    render() {
        const {navigate,white} = this.props;
        return (
            <Ripple
                onPress={
                    () => {
                        Actions.drawerOpen();
                    }
                }
            >
                <View style={styles.container}>
                    <Nucleo name={"menu-right"}  size={17} color="#fff" />
                </View>
            </Ripple>
        );
    }
}



export default MenuButtonRight;
