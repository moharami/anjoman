import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 20
    },
    master: {
        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        right: 0

    },
    scroll:{
        marginTop: 81,
        marginBottom: 46
    },
    postType: {
        flex: 1,
        backgroundColor: 'rgb(41, 49, 52)',
        height: 35,
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        position: 'absolute',
        top: 46,
        right: 0,
        left: 0,
        zIndex: 9999,

    },
    articleContainer: {
        // marginRight: 60,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    titleContainer: {
        backgroundColor: 'rgb(241, 241, 241)',
        padding: 5
    },
    title: {
        color: 'black',
        fontSize: 11,
        paddingBottom: 5
    },
    titleContent: {
        color: 'black',
    },
    textareaContainer: {
        height: 300,
        backgroundColor: 'rgb(241, 241, 241)',
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    textarea: {
        width: '100%',
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 12,
        // backgroundColor: 'rgb(241, 241, 241)',

    },
    article: {
        color: 'black',
        fontSize: 11,
        paddingBottom: 5,
        paddingTop: 10
    },
    footer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 25
    },
    postButton: {
        backgroundColor: 'rgb(21, 138, 191)',
        padding: 5,
        flexDirection: 'row'
    },
    postText: {
        color: 'white',
        paddingLeft: 5,
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 12
    },
    addImage: {
        color: 'black',
        fontSize: 12,
        paddingRight: 13
    },
    clearContainer: {
        paddingRight: 13,
        paddingLeft: 13,
        borderLeftColor: 'black',
        borderLeftWidth: 1
    },
    clear: {
        color: 'black',
        fontSize: 12
    },
    ImageContainer: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CDDC39',
        marginRight: 10,
        marginTop: 10

    },
    photoContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    button: {
        // width: '25%',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        borderRightColor: 'white',
        borderRightWidth: 10,
        backgroundColor: 'rgb(244, 244, 244)',
        marginTop: 10,
        borderRadius: 5,
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonText: {
        color: 'gray',
        fontSize: 12
    },
    addButtonContainer: {

        justifyContent: "center",
        alignItems: "center",
        padding: 20
    },
    addButton: {
        width: '100%',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(224, 224, 224)',

    },
    addText: {
        color: 'gray',
        fontSize: 12
    }
});
