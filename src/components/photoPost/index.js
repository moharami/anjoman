import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView,TextInput, AsyncStorage} from 'react-native';
import styles from './styles'
import Nucleo from '../nucleo'
import Textarea from 'react-native-textarea';
import ImagePicker from 'react-native-image-picker';
import Prompt from 'react-native-prompt';
import Axios from 'axios'
import Loader from '../../components/loader'
import Items from '../../components/footerItems'
import Ripple from 'react-native-material-ripple';
import {connect} from 'react-redux';
import bg from '../../assets/Logo.png';
import {Actions} from 'react-native-router-flux'

class PhotoPost extends Component {
    constructor(props){
        super(props);
        this.state = {
            title: '',
            content: '',
            ImageSource: null,
            promptVisible: false,
            imageCount: "",
            imageArray: [],
            addImage: false,
            imageData: [],
            loading: false
        };
    }
    selectPhotoNumbers() {
        this.setState({promptVisible: true});
    }
    selectPhotoTapped(item) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                let imgArray = this.state.imageArray;
                let imgData = this.state.imageData;
                imgArray[item.id-1].src = source;
                imgData[item.id-1] = response.uri;

                this.setState({
                    imageArray: imgArray,
                    imageData: imgData
                });
            }
        });
    }
    submit(value){
        let countArray = [], i;
        for ( i = 1; i <= Number(value); i++) {
            countArray.push({id: i, src: ''});
        }
        this.setState({
            imageCount: value,
            addImage: true,
            promptVisible: false,
            imageArray: countArray
        })
    }
    async articlePost(){
        console.log('images', this.state.imageData[0]);
        const information = await AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            if(newInfo.token !== null) {
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token ;
                Axios.defaults.headers.common['Content-Type'] = 'multipart/form-data' ;
                console.log('images', this.state.imageData)
                this.setState({loading: true});
                let formdata = new FormData();

                formdata.append("postable_type", "App\\User")
                formdata.append("postable_id", this.props.user.id)
                formdata.append("title", this.state.title)
                formdata.append("content", this.state.content)
                formdata.append("type", "image");

                for (let i=0; i < this.state.imageData.length; i++) {
                    // formdata.append("image", 'data:image/jpeg;base64,'+ this.state.imageData[i])
                    formdata.append("image", {
                            uri: this.state.imageData[i],
                            type: 'image/jpeg',
                            name: 'image'
                        },
                    )
                }
                Axios.post('/post', formdata,
                ).then(response=> {
                    this.setState({loading: false});
                    alert('your post added successfully');
                    console.log(response);
                }).catch((error)=> {
                        this.setState({loading: false});
                        alert('some problem ocured');
                        console.log(error.response);
                    }
                );
            }
        });
    }
    clearPost(){
        this.setState({title: '', content: ''})
    }
    otherPost(num){
        if(num === 0){
            Actions.push('ArticlePost');
        }
        else{
            Actions.push('VideoPost');
        }
    }
    render() {
        const {user} = this.props;

        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Text style={{color: 'white', fontSize: 18, paddingLeft: 20, fontWeight: 'bold'}}>New Post</Text>
                    <Image style={{width: 90, height: 20, resizeMode: 'contain', marginRight: 10}} source={bg} />
                </View>
                <View style={styles.postType}>
                    <Ripple style={{ width: '34%',flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(41, 49, 52)'}} onPress={()=> this.otherPost(0)}>
                        <Nucleo style={{color: 'rgb(75, 80, 84)', fontSize: 12, paddingRight: 4}} name='edit-74' />
                        <Text style={{color: 'rgb(75, 80, 84)', fontSize: 12 }}>Write an Article</Text>
                    </Ripple>
                    <Ripple style={{width: '33%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(27, 32, 35)'}} >
                        <Nucleo style={{color: 'rgb(19, 122, 167)', fontSize: 12, paddingRight: 4}} name='picture' />
                        <Text style={{color: 'rgb(19, 122, 167)', fontSize: 12}}>Photo / Album</Text>
                    </Ripple>
                    <Ripple style={{width: '33%', flexDirection: 'row' ,alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(41, 49, 52)'}} onPress={()=> this.otherPost(1)}>
                        <Nucleo style={{color: 'rgb(75, 80, 84)', fontSize: 12, paddingRight: 4}} name='action-74' />
                        <Text style={{color: 'rgb(75, 80, 84)', fontSize: 12}}>Video</Text>
                    </Ripple>
                </View>
                <ScrollView style={styles.scroll}>
                    {
                        this.state.loading ? <Loader send={false}/> : (

                            <View style={styles.container}>
                                <Text style={styles.title}>Title</Text>
                                <TextInput
                                    placeholder="title"
                                    underlineColorAndroid='transparent'
                                    value={this.state.title}
                                    style={{
                                        height: 32,
                                        backgroundColor: 'rgb(241, 241, 241)',
                                        paddingLeft: 5,
                                        paddingTop: 8,
                                        width: '100%'
                                    }}
                                    onChangeText={(title) => this.setState({title})}/>
                                <Text style={styles.article}>Article</Text>
                                <View style={styles.articleContainer}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            defaultValue={this.state.content}
                            maxLength={120}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            value={this.state.content}
                            onChangeText={(content) => this.setState({content})}
                        />
                                </View>
                                <View style={styles.footer}>
                                    <TouchableOpacity onPress={this.selectPhotoNumbers.bind(this)}>
                                        <View>
                                            {
                                                !this.state.addImage ? <Text style={styles.addImage}>Add Image</Text> : null
                                            }
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.clearPost()}>
                                        <View style={styles.clearContainer}>
                                            <Text style={styles.clear}>Clear Text</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.articlePost()}>
                                        <View style={styles.postButton}>
                                            <Nucleo style={{color: '#fff', fontSize: 14, paddingLeft: 5, paddingTop: 2}}
                                                    name='send'/>
                                            <Text style={styles.postText}>Post to Timeline</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {this.state.imageCount !== '' ?
                                    <View style={styles.photoContainer}>
                                        {
                                            this.state.imageArray.map((item, index) => {
                                                if (item.src === '') {
                                                    return <TouchableOpacity key={index} style={styles.button}
                                                                             onPress={this.selectPhotoTapped.bind(this, item)}>
                                                        <Text style={styles.buttonText}>select photo</Text>
                                                    </TouchableOpacity>
                                                }
                                                else return <Image style={styles.ImageContainer} source={item.src}
                                                                   key={index}/>
                                            })
                                        }
                                    </View>
                                    : null
                                }
                                {/*<Prompt*/}
                                    {/*title="image count"*/}
                                    {/*placeholder="inter your number"*/}
                                    {/*defaultValue="0"*/}
                                    {/*visible={ this.state.promptVisible }*/}
                                    {/*onCancel={ () => this.setState({*/}
                                        {/*promptVisible: false,*/}
                                        {/*message: "You cancelled"*/}
                                    {/*})}*/}
                                    {/*onSubmit={(value) => this.submit(value)}/>*/}
                            </View>
                        )
                    }
                </ScrollView>
                <Items active={false} />
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,

    }
}
export default connect(mapStateToProps)(PhotoPost);
