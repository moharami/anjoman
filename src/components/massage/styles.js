import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        padding: 12,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray',
        backgroundColor: 'white'
    },
    header: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "flex-start",
    },
    iconText: {
        color: 'gray',
        fontSize: 10,
        textAlign: 'right'
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '80%'
    },
    image:{
        borderRadius: 90,
        width: 65,
        height: 65,
        marginRight: 10,
        borderWidth: 2,
        borderColor: 'white'
    },
    content: {
        fontSize: 10
    },
    title: {
        fontSize: 10,
        fontWeight: 'bold'
    },
    date: {
        fontSize: 8,
        color: '#158abf'
    }
});
