import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        marginRight: 25,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 220,
        height: 200,
        flex:1,
        resizeMode:'contain',
    },
    caption: {
        fontSize: 15,
        color: 'rgb(107, 104, 104)',
        fontWeight: 'bold',
    },
    date: {
        fontSize: 13,
        color: 'rgb(150, 150, 150)',
        fontWeight: 'bold',
        paddingTop: 3
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        paddingTop: 15
    },
    iconContainerBlue: {
        height: 24,
        width: 24,
        borderRadius: 24,
        backgroundColor: "rgb(52, 117, 183)",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 5,
        marginLeft: 5

    },
    iconContainerGreen: {
        height: 24,
        width: 24,
        borderRadius: 24,
        backgroundColor: "rgb(29, 198, 177)",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 5,
        marginLeft: 5

    },
    eyeContainer: {
        height: 24,
        width: 24,
        borderRadius: 24,
        backgroundColor: "rgb(245, 245, 245)",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 5,
        marginLeft: 5
    },
    iconContent: {
        backgroundColor: "transparent",
        fontWeight: "bold",
        fontSize: 12,
        color: "white",
    },
    eyeContent: {
        backgroundColor: "transparent",
        fontWeight: "bold",
        fontSize: 12,
    }
});
