import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles'
import image from '../../assets/cpost1.jpg'
import Nucleo from '../nucleo'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class PostsSliderItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={image} />
                <Text style={styles.caption}>Gallery Name Comes Here</Text>
                <Text style={styles.date}>July 2018</Text>
                <View style={styles.left}>
                    <View style={styles.iconContainerBlue}>
                        <Nucleo style={styles.iconContent} name={"share-2"} size={7} color="#fff" />
                    </View>
                    <Text>{this.props.share}</Text>
                    <View style={styles.iconContainerGreen}>
                        <Nucleo style={styles.iconContent} name={"heart-2"} size={7} color="#fff" />
                    </View>
                    <Text>{this.props.likes}</Text>
                    <View style={styles.iconContainerBlue}>
                        <Nucleo style={styles.iconContent} name={"chat-45"} size={7} color="#fff" />
                    </View>
                    <Text>{this.props.comments}</Text>
                    <View style={styles.eyeContainer}>
                        {/*<Nucleo style={styles.iconContent} name={"eye-19"} size={7} color="rgb(20, 122, 170)" />*/}
                        <Icon name="eye" size={14} color="rgb(20, 122, 170)" />

                    </View>
                    <Text>{this.props.seen}</Text>
                </View>
            </View>
        );
    }
}
export default PostsSliderItem;