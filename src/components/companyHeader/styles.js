
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    companyProfileHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    companyProfileTitle: {
        color: 'black',
        fontSize: 12,
        fontWeight: 'bold'
    },
});
