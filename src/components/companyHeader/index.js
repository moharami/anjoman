import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles'
import Nucleo from '../nucleo'

class CompanyHeader extends Component {
    render() {
        return (
            <View style={styles.companyProfileHeader}>
                <Text style={styles.companyProfileTitle}> >  {this.props.title}</Text>
                <Nucleo style={{color: 'black', fontSize: 11, paddingRight: 8, paddingBottom: 7}} name="single-01"/>
            </View>
        );
    }
}
export default CompanyHeader;