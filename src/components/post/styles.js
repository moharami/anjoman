import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray'
    },
    header: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    },
    right: {
        flexDirection:'row',
        justifyContent: "flex-end",
        alignItems: "center",
    },
    date: {
        color: 'gray',
        fontSize: 10,
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
    },
    image:{
        borderRadius: 90,
        width: 30,
        height: 30,
        marginRight: 10
    },
    name: {
        fontSize: 10,
        fontWeight: 'bold'
    },
    content: {
        fontSize: 12,
        lineHeight: 20
    },
    iconContainerBlue: {
        height: 20,
        width: 20,
        borderRadius: 20,
        backgroundColor: "rgb(52, 117, 183)",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 5,
        marginLeft: 5

    },
    iconContainerGreen: {
        height: 20,
        width: 20,
        borderRadius: 20,
        backgroundColor: "rgb(29, 198, 177)",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 5,
        marginLeft: 5

    },
    iconContent: {
        backgroundColor: "transparent",
        fontWeight: "bold",
        fontSize: 12,
        color: "white",

    },
    footer: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    }
});
