import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import styles from './styles'
import Nucleo from '../nucleo'
// import image from '../../assets/user.png'
import SlideShow from '../slideShow'
import moment from 'moment';

class Post extends Component {
    constructor(props){
        super(props);
    }
    render() {
        const baseImageUrl = "http://professionall.azarinpro.info/company/attachment-retrieve/";
        const slider = this.props.attachments.map((item) => {return {illustration: baseImageUrl+item.uid }});
        const avatarImage = this.props.item.postable  && this.props.item.postable.avatar ? {uri: "http://professionall.azarinpro.info/company/attachment-retrieve/"+ this.props.item.postable.avatar.uid} :  {uri: 'http://professionall.azarinpro.info/uploads/default.png'}
        return (
            <View style={styles.container}>
               <View style={styles.header}>
                    <View style={styles.left}>
                        <Image style={styles.image} source={avatarImage} />
                        <Text style={styles.name} >{this.props.item.postable.fname} {this.props.item.postable.lname}</Text>
                        <Nucleo style={{color: '#000', fontSize: 10, paddingRight: 5, paddingLeft: 5}} name="small-triangle-right"/>
                        <Text style={styles.name}>Public image</Text>
                    </View>
                   <View style={styles.right}>
                       <Text style={styles.date}>{moment(this.props.item.created_at).format('ll')}</Text>
                       <Nucleo style={{color: 'rgb(100,100,100)', fontSize: 14, paddingLeft: 10}} name="menu-right"/>
                   </View>
               </View>
               <View>
                   <Text style={styles.content}>
                       {this.props.item.content}
                   </Text>
               </View>
                {slider &&
                <View style={styles.slider}>
                    <SlideShow items={slider} />
                </View>}
               <View style={styles.footer}>
                    <View style={styles.left}>
                        <View style={styles.iconContainerGreen}>
                            <Nucleo style={styles.iconContent} name={"heart-2"} size={6} color="#fff" />
                        </View>
                        <Text>{this.props.item.likes}</Text>
                        <View style={styles.iconContainerBlue}>
                            <Nucleo style={styles.iconContent} name={"chat-45"} size={6} color="#fff" />
                        </View>
                        <Text>{this.props.item.comments}</Text>
                    </View>
                    <View style={styles.right}>
                        <View style={styles.iconContainerBlue}>
                            <Nucleo style={styles.iconContent} name={"share-2"} size={6} color="#fff" />
                        </View>
                        <Text>{this.props.item.share}</Text>
                    </View>
               </View>
            </View>
        );
    }
}
export default Post;