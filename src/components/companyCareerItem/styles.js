import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        marginRight: 35,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: '100%',
        resizeMode:'contain',
        marginBottom: 20,
        marginTop: 30
    },
    caption: {
        fontSize: 15,
        color: 'rgb(107, 104, 104)',
        fontWeight: 'bold',
        paddingTop: 8
    },
    date: {
        fontSize: 13,
        color: 'rgb(150, 150, 150)',
        fontWeight: 'bold',
        paddingTop: 5
    },
    footer: {
        fontSize: 10,
        color: 'rgb(150, 150, 150)',
        fontWeight: 'bold',
        paddingTop: 10,
        fontStyle: 'italic'
    },
    details: {
        width: '100%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(245, 245, 245)',
        borderRadius: 3,
        marginRight: 10,

    },
    detailsText: {
        color: 'rgb(50, 152, 200)',
        fontSize: 12,
        paddingLeft: 8
    },
});
