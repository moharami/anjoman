import React, {Component} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import styles from './styles'
import image from '../../assets/career-pic2.png'
import Nucleo from '../nucleo'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class CareerItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={image} />
                <View style={styles.details}>
                    <Icon name="check" size={12} color="rgb(50, 152, 200)" />
                    <Text style={styles.detailsText}>Apply</Text>
                </View>
                <Text style={styles.footer}>Applied by 32  other</Text>
            </View>
        );
    }
}
export default CareerItem;