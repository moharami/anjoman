import React, {Component} from 'react';
import {View, Text, TouchableHighlight} from 'react-native';
import Swipeable from 'react-native-swipeable';
import styles from './styles'
import Notification from "../notification/index";
import SwipeIcon from '../swipeIcon'
class SwipeableNotification extends Component {
    constructor(props){
        super(props);
    }

    render() {
        // const leftContent = <Notification />;
        const leftContent = <Text>Pull to activate</Text>;
        const rightButtons = [
            <SwipeIcon icon="sunglasses" label="Make as Unread" backgroundColor="rgb(52, 117, 183)"/>,
            <SwipeIcon icon="trash-simple" label="Archive" backgroundColor="red" />,

        ];
        return (
            <View style={styles.container}>
                <Swipeable rightButtons={rightButtons} >
                    <Notification />
                </Swipeable>
            </View>
        );
    }
}
export default SwipeableNotification;
