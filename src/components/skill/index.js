import React, {Component} from 'react';
import {View, Text} from 'react-native';

import styles from './styles'

class Skill extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.skill}>{this.props.skill}</Text>
                <Text style={styles.rate}>{this.props.rate}</Text>
            </View>
        );
    }
}
export default Skill;