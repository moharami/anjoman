/**
 * Created by user on 5/8/2018.
 */
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'rgb(246, 246, 246)',
        borderRadius: 10,
        marginRight: 10,
        marginBottom: 10,
        padding: 3
    },
    skill: {
        color: 'black',
        fontSize: 12,
        paddingLeft: 10
    },
    rate: {
        fontSize: 12,
        color: 'rgb(52, 117, 183)',
        paddingLeft: 10,
        paddingRight: 5
    }
});
