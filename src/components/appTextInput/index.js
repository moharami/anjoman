import React, {Component} from 'react';
import {ScrollView, TextInput, View,Text} from 'react-native';

class AppTextInput extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    }
    render() {
        return (
            <View>
                <TextInput
                    placeholder="search..."
                    placeholderTextColor="#158abf"
                    underlineColorAndroid='transparent'
                    value={this.state.text}
                    style={{
                        flex: 1,
                        height: 36,
                        backgroundColor: 'rgb(27, 32, 35)',
                        color: '#158abf',
                        paddingLeft: 15,
                        width: '100%',
                        position: 'absolute',
                        zIndex: 9999,
                        top: 46,
                        left: 0,
                        right: 0 }}
                    onChangeText={(text) => this.setState({text})} />
            </View>
        );
    }
}
export default AppTextInput;