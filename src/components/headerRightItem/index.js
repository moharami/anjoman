import React, {Component} from 'react';
import {View, Image} from 'react-native';
import MenuButtonRight from '../menuButtonRight'
import MenuButtonLeft from '../menuButtonLeft'
import styles from './style'

class HeaderRightItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <MenuButtonLeft />
                <MenuButtonRight />
            </View>
        );
    }
}
export default HeaderRightItem;