/**
 * Created by user on 5/8/2018.
 */
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    // image: {
    //     marginRight: 60
    // }
});
