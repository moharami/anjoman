import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView, TextInput, AsyncStorage} from 'react-native';
import styles from './styles'
import Nucleo from '../nucleo'
import Textarea from 'react-native-textarea';
import ImagePicker from 'react-native-image-picker';
import VideoPlayer from 'react-native-video-controls';
import Axios from 'axios'
import Loader from '../../components/loader'
import Items from '../../components/footerItems'
import Ripple from 'react-native-material-ripple';
import bg from '../../assets/Logo.png';
import {Actions} from 'react-native-router-flux'

class PhotoPost extends Component {
    constructor(props){
        super(props);
        this.state = {
            videoSelected: false,
            title: '',
            content: '',
            video: null,
            videoData: '',
            loading: false
        };
    }
    selectVideoTapped() {
        const options = {
            mediaType: 'video',
            videoQuality: 'low',
            waitUntilSaved: 10000 ,
            quality: 1.0,

            title: 'Video Picker',
            takePhotoButtonTitle: 'Take Video...',
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
             else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                if(response){
                    // const merged = "file:/"+ response.path;
                    const source = { uri: response.uri};
                    this.setState({
                        video: source,
                        videoSelected: true,
                        videoData: response
                    });
                    console.log('adddress ', this.state.video);
                }else{

                    alert('no video selected');
                }
            }
        });
    }
    async articlePost(){

        console.log('images', this.state.imageData[0]);
        const information = await AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            if(newInfo.token !== null) {
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token ;
                Axios.defaults.headers.common['Content-Type'] = 'multipart/form-data' ;
                console.log('images', this.state.imageData)
                this.setState({loading: true});


                const uri = this.state.videoData;
                const uriParts = uri.path.split('.');
                const fileType = uriParts[uriParts.length - 1];
                // const formData = new FormData();

                let formdata = new FormData();

                formdata.append("postable_type", "App\\User")
                formdata.append("postable_id", this.props.user.id)
                formdata.append("title", this.state.title)
                formdata.append("content", this.state.content)
                formdata.append("type", "image");
                formdata.append("photo",
                    {
                        uri: this.state.videoData.uri,
                        name: `photo.${fileType}`,
                        type: `image/${fileType}`,
                    }
                )

                Axios.post('/post', formdata,
                ).then(response=> {
                    this.setState({loading: false});
                    alert('your post added successfully');
                    console.log(response);
                }).catch((error)=> {
                        this.setState({loading: false});
                        alert('some problem ocured');
                        console.log(error.response);
                    }
                );
            }
        });
    }
    clearPost(){
        this.setState({title: '', content: ''})
    }
    otherPost(num){
        if(num === 0){
            Actions.push('ArticlePost');
        }
        else{
            Actions.push('PhotoPost');
        }
    }
    render() {
        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Text style={{color: 'white', fontSize: 18, paddingLeft: 20, fontWeight: 'bold'}}>New Post</Text>
                    <Image style={{width: 90, height: 20, resizeMode: 'contain', marginRight: 10}} source={bg} />
                </View>
                <View style={styles.postType}>
                    <Ripple style={{ width: '34%',flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(41, 49, 52)'}} onPress={()=> this.otherPost(0)}>
                        <Nucleo style={{color: 'rgb(75, 80, 84)', fontSize: 12, paddingRight: 4}} name='edit-74' />
                        <Text style={{color: 'rgb(75, 80, 84)', fontSize: 12 }}>Write an Article</Text>
                    </Ripple>
                    <Ripple style={{width: '33%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor:  'rgb(41, 49, 52)'}} onPress={()=> this.otherPost(1)} >
                        <Nucleo style={{color: 'rgb(75, 80, 84)', fontSize: 12, paddingRight: 4}} name='picture' />
                        <Text style={{color: 'rgb(75, 80, 84)', fontSize: 12}}>Photo / Album</Text>
                    </Ripple>
                    <Ripple style={{width: '33%', flexDirection: 'row' ,alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(27, 32, 35)'}} >
                        <Nucleo style={{color: 'rgb(19, 122, 167)', fontSize: 12, paddingRight: 4}} name='action-74' />
                        <Text style={{color: 'rgb(19, 122, 167)', fontSize: 12}}>Video</Text>
                    </Ripple>
                </View>
                <ScrollView style={styles.scroll}>
                    {
                        this.state.loading ? <Loader send={false}/> : (
                    <View style={styles.container}>
                        <Text style={styles.title}>Title</Text>
                        <TextInput
                            placeholder="title"
                            underlineColorAndroid='transparent'
                            value={this.state.title}
                            style={{ height: 32,  backgroundColor: 'rgb(241, 241, 241)', paddingLeft: 5, paddingTop: 8, width: '100%' }}
                            onChangeText={(title) => this.setState({title})} />
                        <Text style={styles.article}>Article</Text>
                        <View style={styles.articleContainer}>
                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            defaultValue={this.state.content}
                            maxLength={120}
                            placeholderTextColor={'#c7c7c7'}
                            underlineColorAndroid={'transparent'}
                            value={this.state.content}
                            onChangeText={(content) => this.setState({content})}
                        />
                        </View>
                        <View style={styles.footer}>
                            <TouchableOpacity onPress={this.selectVideoTapped.bind(this)}>
                                <View>
                                    {
                                        this.state.video === null ? <Text style={styles.addImage} >Add Video</Text> : null
                                    }
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.clearPost()}>
                                <View style={styles.clearContainer}>
                                    <Text style={styles.clear} >Clear Text</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.articlePost()}>
                                <View style={styles.postButton}>
                                    <Nucleo style={{color: '#fff', fontSize: 14, paddingLeft: 5, paddingTop: 2}} name='send' />
                                    <Text style={styles.postText}>Post to Timeline</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                            {this.state.videoSelected ? <VideoPlayer source={this.state.video} />: null}
                    </View>
                        )
                    }
                </ScrollView>
                <Items active={false} />
            </View>
        );
    }
}
export default PhotoPost;