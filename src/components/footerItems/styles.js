
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        backgroundColor: 'rgb(41, 49, 52)',
        height: 46,
        flexDirection: 'row',
        position: 'absolute',
        right: 0,
        left: 0,
        bottom: 0,
        zIndex: 9999,


    }
});
