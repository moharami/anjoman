import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Item from '../../components/footerNavigationItem'
import styles from './styles'
import {Actions} from 'react-native-router-flux'

class Items extends Component {
    constructor(props){
        super(props);

    }
    activeStyle(num){
        switch (num){
            case 0: {
                Actions.push('FriendTabHome');
            }
                break;
            case 1: {
                Actions.push('Notifications');
            }
                break;
            case 2:
                Actions.push('Massages');
                break;
            case 3:
                Actions.push('Contacts');
                break;
            case 4:
                Actions.push('JobOffers');
                break;
            case 5:
                Actions.push('Profile');
                break;
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Item icon="home-52" label="Home" notification={true} notificationCount="3" active={this.props.active === 1} handlePress={()=> this.activeStyle(0)} />
                <Item icon="bell-53" label="Notifications" notification={true} notificationCount="3" active={this.props.active === 2}  handlePress={()=> this.activeStyle(1)} />
                <Item icon="chat-46" label="Massages" notification={true} notificationCount="3" active={this.props.active === 3}   handlePress={()=> this.activeStyle(2)} />
                <Item icon="multiple-11" label="Contacts" notification={false} active={this.props.active === 4}  handlePress={()=> this.activeStyle(3)} />
                <Item icon="briefcase-24" label="Job Offers" notification={false} active={this.props.active === 5}  handlePress={()=> this.activeStyle(4)} />
                <Item icon="man" label="Profile" notification={false} active={this.props.active === 6} handlePress={()=> this.activeStyle(5)} />
            </View>
        );
    }
}
export default Items;