import React, {Component} from 'react';
import {View, Text,StyleSheet} from 'react-native';
import Nucleo from '../nucleo'
import {Actions} from 'react-native-router-flux'

import Ripple from "react-native-material-ripple";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
});

class ComposeIcon extends Component {
    countCounter = () => {
        return 0
    };

    render() {
        const {navigate} = this.props;
        return (
            <Ripple
                onPress={() =>
                    Actions.push('ArticlePost')
                }
            >
                <View style={styles.container}>
                    <Nucleo style={{color: this.props.color, fontSize: 18, paddingLeft: 10}} name='edit-74' />
                </View>
            </Ripple>
        );
    }
}

export default ComposeIcon;