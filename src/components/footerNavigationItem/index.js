import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Ripple from 'react-native-material-ripple';

import styles from './styles'
import Nucleo from '../nucleo'

class Item extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <Ripple style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: this.props.active ? 'rgb(0, 113, 173)': 'transparent'
            }}
            onPress={this.props.handlePress}
            >
                <View>
                    <Nucleo style={{color: '#fff', fontSize: 14}} name={this.props.icon}/>
                    {
                        this.props.notification ?
                            <View style={styles.logoContainer}>
                                <Text style={styles.logoText}>{this.props.notificationCount}</Text>
                            </View> : null
                    }
                </View>
                <Text style={styles.label}>{this.props.label}</Text>
            </Ripple>
        );
    }
}
export default Item;