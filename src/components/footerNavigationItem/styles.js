/**
 * Created by user on 5/8/2018.
 */
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },
    logoContainer: {
        height: 15,
        width: 15,
        borderRadius: 20,
        backgroundColor: "red",
        justifyContent: "center",
        alignItems: "center",
        position: 'absolute',
        top: -6,
        right: -8,

    },
    logoText: {
        backgroundColor: "transparent",
        fontWeight: "bold",
        fontSize: 12,
        color: "white",
    },
    label: {
        color: 'white',
        fontSize: 7
    }
});
