import React, {Component} from 'react';
import { Text, View, ScrollView, TextInput, AsyncStorage} from 'react-native';
import styles from './styles'
import Ripple from 'react-native-material-ripple';
import Axios from 'axios'
import Loader from '../../components/loader'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://professionall.azarinpro.info/api';
Axios.defaults.baseURL = url;
class Signup extends Component {
    constructor(props){
        super(props);
        this.state = {
            password: '',
            firstName: '',
            lastName: '',
            email: '',
            passConfirmation: '',
            mobile: '',
            loading: false
        };
    }
    signUp(){
        if(this.state.email === '' || this.state.password === '' || this.state.firstName === '' || this.state.lastName ==='' || this.state.mobile === '' || this.state.passConfirmation === '')
            alert('please fill required fields')
        else {
            this.setState({loading: true});
            Axios.post('/signup', {
                email: this.state.email,
                password: this.state.password,
                fname: this.state.firstName,
                lname: this.state.lastName,
                mobile: this.state.mobile,
                password_confirmation: this.state.passConfirmation
            }).then(response=> {
                this.setState({loading: false});
                console.log('signup resss', response)
                if(response.data === 'created'){
                    console.log('signup token', response.request._headers.authorization)
                    const subToken = response.request._headers.authorization.substring(7);
                    AsyncStorage.setItem('token', subToken);
                    this.props.navigation.navigate('main');
                }
                else {
                    alert(response.data.errors)
                }
            })
                .catch( (error)=> {
                    this.setState({loading: false});
                    alert('invalid data try again');
                        console.log(error.response);
                    }
                );
        }
    }
    render() {
        return (
            <ScrollView style={styles.scroll}>
                <View style={styles.header}>
                    <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: 'white'}}>Sign Up</Text>
                </View>
                {
                    this.state.loading ? <Loader send={false} /> : (
                        <View style={styles.container}>

                            <View style={styles.userContainer}>
                                <Text style={styles.Text}>Email: </Text>
                                <TextInput
                                    placeholder="email"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.email}
                                    style={{height: 40, color: '#158abf', paddingLeft: 15, width: '70%' }}
                                    onChangeText={(email) => this.setState({email})} />
                                {this.state.email=== '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.userContainer}>
                                <Text style={styles.Text}>Password: </Text>
                                <TextInput
                                    placeholder="password"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.password}
                                    style={{height: 40, color: '#158abf', paddingLeft: 15, width: '70%' }}
                                    onChangeText={(password) => this.setState({password})} />
                                {this.state.password === '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.userContainer}>
                                <Text style={styles.Text}>First Name: </Text>
                                <TextInput
                                    placeholder="first name"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.firstName}
                                    style={{height: 40, color: '#158abf', paddingLeft: 15, width: '70%' }}
                                    onChangeText={(firstName) => this.setState({firstName})} />
                                {this.state.firstName === '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.userContainer}>
                                <Text style={styles.Text}>Last Name: </Text>
                                <TextInput
                                    placeholder="last name"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.lastName}
                                    style={{height: 40, color: '#158abf', paddingLeft: 15, width: '70%' }}
                                    onChangeText={(lastName) => this.setState({lastName})} />
                                {this.state.lastName=== '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.userContainer}>
                                <Text style={styles.Text}>Mobile: </Text>
                                <TextInput
                                    placeholder="mobile"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.mobile}
                                    style={{height: 40, color: '#158abf', paddingLeft: 15, width: '70%' }}
                                    onChangeText={(mobile) => this.setState({mobile})} />
                                {this.state.mobile=== '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.userContainer}>
                                <Text style={styles.Text}>pass confirmation: </Text>
                                <TextInput
                                    placeholder="confirmation"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.passConfirmation}
                                    style={{ height: 40, color: '#158abf', paddingLeft: 15, width: '57%'}}
                                    onChangeText={(passConfirmation) => this.setState({passConfirmation})} />
                                {this.state.passConfirmation === '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.buttonContainer}>
                                <View style={styles.button}>
                                    <Ripple onPress={()=>this.signUp()}>
                                        <Text style={styles.buttonText}>Register</Text>
                                    </Ripple>
                                </View>
                            </View>
                        </View>
                    )
                }
            </ScrollView>
        );
    }
}
export default Signup;