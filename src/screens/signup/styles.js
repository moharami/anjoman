import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 60,
        paddingLeft: 30,
        paddingRight: 30
    },
    header: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46
    },
    userContainer: {
        width: '100%',
        flexDirection:'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingBottom: 5,
    },
    button: {
        width: '50%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        borderRightColor: 'white',
    },
    buttonText: {
        color: 'white',
        fontSize: 14
    },
    buttonContainer: {
        width: '45%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgb(41, 49, 52)',
        marginTop: 15
    },
    Text: {
        color: 'black',
        fontWeight: 'bold',
    }
});
