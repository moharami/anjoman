import React, {Component} from 'react';
import { Text, View, ScrollView, TextInput, AsyncStorage} from 'react-native';
import styles from './styles'
import Ripple from 'react-native-material-ripple';
import Axios from 'axios'
import Loader from '../../components/loader'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://professionall.azarinpro.info/api';
Axios.defaults.baseURL = url;
import {Actions} from 'react-native-router-flux'

class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            login: true,
            loading: false

        };
    }
    async redirect(key){
        this.setState({login: false});
        try {
            let posts = await AsyncStorage.getItem(key).then((info) => {
                const newInfo = JSON.parse(info);
                if(newInfo !== null) {
                    console.log('new info',newInfo);
                    Axios.post('/signin', {
                        email: newInfo.email, password: newInfo.password
                    }).then(response=> {
                        this.setState({loading: true, login: false});
                        if(newInfo.token !== response.data){
                            const updatedInfo = {'token': response.data,  'email': newInfo.email, 'password': newInfo.password};
                            AsyncStorage.setItem('token', JSON.stringify(updatedInfo));
                            Actions.push('FriendTabHome');
                        }
                        else {
                            Actions.push('FriendTabHome');
                        }
                    })
                    .catch((error)=> {
                        this.setState({loading: false, login: true});
                        console.log(error.response);
                        }
                    );
                }
                else{
                    this.setState({login: true});

                }
            });

        } catch (error) {
            console.log(error)
        }
    }
    // componentWillMount(){
    //     this.redirect('token');
    // }
    login() {
        if(this.state.email === '' || this.state.password === '')
            alert('please fill the required fields');
        else {
            this.setState({loading: true});
            Axios.post('/signin', {
                email: this.state.email, password: this.state.password
            }).then(response=> {
                console.log('resssssssssssshihi', response)
                this.setState({loading: false, login: false});

                const info = {'token': response.data, 'email': this.state.email, password: this.state.password};
                AsyncStorage.setItem('token', JSON.stringify(info));
                console.log('info', info);
                Actions.push('FriendTabHome');
            })
            .catch((error) => {
                this.setState({loading: false});
                alert('invalid data try again');

                }
            );
        }
    }
    render() {
        if(!this.state.login){
            return (<View></View>)
        }
          else return (
            <ScrollView style={styles.scroll}>
                <View style={styles.header}>
                    <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: 'white'}}>Login</Text>
                </View>
                {
                    this.state.loading ? <Loader send={false} /> : (
                        <View style={styles.container}>
                            <View style={styles.userContainer}>
                                <Text style={styles.Text}>Email: </Text>
                                <TextInput
                                    placeholder="email"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.text}
                                    style={{ height: 40, color: '#158abf', paddingLeft: 15, width: '70%'}}
                                    onChangeText={(email) => this.setState({email})} />
                                {this.state.email === '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.passContainer}>
                                <Text style={styles.Text}>Password: </Text>
                                <TextInput
                                    placeholder="password"
                                    placeholderTextColor="#158abf"
                                    underlineColorAndroid='rgb(41, 49, 52)'
                                    value={this.state.text}
                                    style={{height: 40, color: '#158abf', paddingLeft: 15, width: '70%' }}
                                    onChangeText={(password) => this.setState({password})} />
                                {this.state.password === '' ? <Text style={{color: 'red'}}>* </Text>: null}
                            </View>
                            <View style={styles.buttonContainer}>
                                <View style={styles.button}>
                                    <Ripple onPress={()=> this.login()}>
                                        <Text style={styles.buttonText}>Login</Text>
                                    </Ripple>
                                </View>
                                <View style={styles.button}>
                                    <Ripple onPress={()=> Actions.push('Signup')}>
                                        <Text style={styles.buttonText}>Sign up</Text>
                                    </Ripple>
                                </View>
                            </View>
                        </View>
                    )
                }
            </ScrollView>
        );
    }
}
export default Login;