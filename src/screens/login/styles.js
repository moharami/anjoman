import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 100
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46
    },
    userContainer: {
        width: '70%',
        flexDirection:'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingBottom: 5,

    },
    passContainer: {

        width: '70%',
        flexDirection:'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingBottom: 5,
    },
    button: {
        width: '50%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        borderRightColor: 'white',
        borderRightWidth: 10,
    },
    buttonText: {
        color: 'white',
        fontSize: 14
    },
    buttonContainer: {
        width: '70%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgb(41, 49, 52)',
        marginTop: 15

    },
    Text: {
        color: 'black',
        fontWeight: 'bold',
        alignSelf: 'center'
    }
});
