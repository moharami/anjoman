import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        position: 'relative',
        zIndex: 0,
        backgroundColor: 'white'
    },
    // header: {
    //     flex: 1,
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'space-around',
    //     backgroundColor: 'rgb(20, 122, 170)',
    //     height: 46
    // },
    master: {
        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        right: 0

    },
    scroll:{
        marginTop: 46,
        marginBottom: 46
    },
    image: {
        width: '100%',
        height: 150,
        zIndex: 1,
        resizeMode: 'cover',
        overflow: 'visible'
    },
    profileImage: {
        width: 130,
        height: 130,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: 'white',
        position: 'absolute',
        top: 50,
        right: '30%',
        zIndex: 2
    },
    headerLabel: {
        marginLeft: 20
    },
    name: {
        fontWeight: 'bold',
        fontSize: 21,
        alignSelf: 'center',
        color: 'black'
    },
    bio: {
        color: 'black',
        fontSize: 12,
        textAlign: 'center',
        padding: 10,
        backgroundColor: 'white'
    },
    education: {
        fontSize: 10,
        textAlign: 'center',
        paddingRight: 60,
        paddingLeft: 60,
        lineHeight: 15,
        paddingBottom: 10
    },
    Tittle:{
        color: 'rgb(52, 117, 183)',
        paddingBottom: 7
    },
    info: {
        padding: 20,
        borderTopColor: 'lightgray',
        borderTopWidth: 1,
        backgroundColor: 'white'
    },
    Content: {
        fontSize: 10,
        color: 'black',
        lineHeight: 15
    },
    body: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    }

});
