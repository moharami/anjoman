import React, {Component} from 'react';
import { Text, View, ScrollView, Image, ImageBackground} from 'react-native';
import styles from './styles'
import image from '../../assets/profileBg.png'
import profileImage from '../../assets/profilePic.png'
import Nucleo from '../../components/nucleo'
import Skill from '../../components/skill'
import HeaderRightItem from '../../components/headerRightItem'
import bg from '../../assets/Logo.png';
import HeaderLabel from '../../components/headerLabel'
import Items from '../../components/footerItems'
import Ripple from 'react-native-material-ripple'
import {connect} from 'react-redux';

class Profile extends Component {
    render() {
        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Image style={{width: 90, height: 20, flex: 1, resizeMode: 'contain', marginLeft: 10}} source={bg} />
                    <View style={styles.headerLabel}>
                        <HeaderLabel label="Danial Day-Lewis" notification={false} />
                    </View>
                    <HeaderRightItem navigate={this.props.navigation.navigate}/>
                </View>

                <ScrollView style={styles.scroll}>
                    <View style={styles.container}>

                        <View style={{height: 200}}>
                            <Image style={styles.image} source={image} />
                            <Image style={styles.profileImage} source={profileImage} />
                            <Nucleo style={{color: 'rgb(52, 117, 183)', fontSize: 18, paddingRight: 20,paddingTop: 10, alignSelf: 'flex-end'}} name="edit-76"/>
                        </View>
                        <Text style={styles.name}>Danial Day-lewis</Text>
                        <Text style={styles.bio}>research assistant at normal instute of geneetic engineering and biotechnologyst </Text>
                        <Text style={styles.education}>research assistant at normal instute of geneetic engineering and biotechnologyst </Text>
                        <View style={styles.info}>
                            <Text style={styles.Tittle}> > More About Me</Text>
                            <Text style={styles.Content}>
                                Amir bageri  Usually, contents.
                                something that is contained:
                                the contents of a box.   something that is contained:
                                the contents of a box.  the contents of a box.   something that is contained:
                                the contents of a box.
                            </Text>
                        </View>
                        <View style={styles.info}>
                            <Text style={styles.Tittle}> > Skills</Text>
                            <View style={styles.body}>
                                <Skill skill="Phptoshop" rate="128+" />
                                <Skill skill="illustrator" rate="128+" />
                                <Skill skill="js" rate="128+" />
                                <Skill skill="roby" rate="128+" />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <Items active={6}/>
            </View>

        );
    }
}
export default Profile;