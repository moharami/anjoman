import React, {Component} from 'react';
import {ScrollView, TextInput, View,Text, Image, ImageBackground} from 'react-native';
import Contact from '../../components/contact'
import Nucleo from '../../components/nucleo'
import Ripple from 'react-native-material-ripple';
import Items from '../../components/footerItems'
import styles from './styles'
import bg from '../../assets/Logo.png';
import bg2 from '../../assets/tehranPic.jpg';
import bg3 from '../../assets/eskanto.png';
import bg4 from '../../assets/people3.jpg';
import slide from '../../assets/honor1.jpg';
import HeaderLabel from "../../components/headerLabel/index";
import HeaderRightItem from '../../components/headerRightItem'
import Member from '../../components/companyMember';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import CompanyHeader from '../../components/companyHeader'
import Auction from '../../components/companyAuction'
import SearchItem from "../../components/searchItem/index";
import PortfolioItem from "../../components/portfolioItem/index";
import PostsSliderItem from '../../components/postsSliderItem'
import RelatedCompanyItems from '../../components/relatedCompanyItems'
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';
import CareerItem from '../../components/companyCareerItem'

class Companies extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    }
    render() {
        const slideItems = [
            {"id": 33996,"imagePath": 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMNPqs2p9MSqWCIYLzpZdr7JOmWD5L7vTHk7UxSvn_zk7EqKZN'},
            {"id": 33964,"imagePath": 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUXDh_gaK6AUkycJ2IBuLXdxKfN-GB4nwObSmKWEhztck-82-u'},
            {"id": 3364,"imagePath": 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUXDh_gaK6AUkycJ2IBuLXdxKfN-GB4nwObSmKWEhztck-82-u'},

        ];
        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Image style={{width: 80, height: 20, flex: 1, resizeMode: 'contain', marginRight: 15}} source={bg} />
                    <HeaderLabel label="Company Page" notification={false} />
                    <HeaderRightItem />
                </View>
                <ScrollView style={styles.scroll}>
                    <ImageBackground source={bg2} style={styles.bgImage} />
                    <View style={styles.imageContentContainer}>
                        <View style={styles.imageContent}>
                            <View style={{alignSelf: 'flex-end'}}>
                                <Nucleo style={{color: 'rgb(20, 122, 170)', fontSize: 16, paddingTop: 22, paddingRight: 20}} name='edit-76'/>
                            </View>
                            <Image style={{width: '50%', height: 50, resizeMode: 'contain'}} source={bg3} />
                            <Text style={styles.title}>Eskano</Text>
                            <Text style={styles.label}>book unique homes and experience a city like a local</Text>
                            <Text style={styles.location}>Tehran-Iran</Text>
                            <View  style={styles.memberContainer}>
                                <Text style={styles.memberNum}>+1259</Text>
                                <Text style={styles.member}>Members</Text>
                            </View>
                            <Text style={styles.followInfo}>19 of yout connections folllow Eskano, including</Text>
                            <View style={styles.followerContainer}>
                                <View style={styles.followerInfo}>
                                    <Image style={{width: 35, height: 35, resizeMode: 'contain'}} source={bg4} />
                                    <Text style={styles.followerName}>Babak Ferdowsi</Text>
                                </View>
                                <View style={styles.followerInfo}>
                                    <Image style={{width: 35, height: 35, resizeMode: 'contain'}} source={bg4} />
                                    <Text style={styles.followerName}>Babak Fer</Text>
                                </View>
                                <View style={styles.followerInfo}>
                                    <Image style={{width: 35, height: 35, resizeMode: 'contain'}} source={bg4} />
                                    <Text style={styles.followerName}>Babak Ferdowsi</Text>
                                </View>
                                <View style={styles.followerInfoMenu}>
                                    <Nucleo style={{color: 'rgb(20, 122, 170)', fontSize: 20, paddingBottom: 10}} name='menu-dots'/>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.buttonContainer}>
                        <View style={styles.jobs}>
                            <Nucleo style={{color: 'rgb(20, 122, 170)', fontSize: 11, paddingRight: 5}} name="briefcase-24"/>
                            <Text style={styles.jobText}>Jobs</Text>
                        </View>
                        <View style={styles.following}>
                            <Nucleo style={{color: '#fff', fontSize: 11, paddingRight: 8}} name="single-01"/>
                            <Text style={styles.followingText}>Following</Text>
                        </View>
                    </View>
                    <View style={styles.companyProfile}>
                        <CompanyHeader title="Company Profile" />
                        <Text style={styles.companyProfileBody}>
                            As of 2017, text messages are used by youth and adults for personal, family and social purposes and in business. Governmental and non-governmental organizations use text messaging for communication between colleagues. As with emailing, in the 2010s, the sending of short informal messages has become an accepted part of many cultures.[1] This makes texting a quick and easy way to communicate with friends and colleagues, including in contexts where a call would be impolite or inappropriate (e.g., calling very late at night or when one knows the other person is busy with family or work activities). Like e-mail and voice mail, and unlike calls (in which the caller hopes to speak directly with the recipient), texting does not require the caller and recipient to both be free at the same moment; this permits communication even between busy individuals. Text messages can also be used to interact with automated systems, for example, to order products or services from e-commerce websites, or to participate in online contests. Advertisers and service providers use direct text marketing to send messages to mobile users about promotions, payment due dates, and other notifications instead of using postal mail, email, or voicemail.
                        </Text>
                    </View>
                    <View style={styles.members}>
                        <View style={styles.membersHeader}>
                            <View style={styles.left}>
                                <Text style={styles.membersTitle}> > Members</Text>
                                <Text style={styles.membersTitleMassage}> [See All 1259 Employees and Followers ]</Text>
                            </View>
                            <Nucleo style={{color: 'black', fontSize: 11, paddingRight: 8, paddingBottom: 7}} name="single-01"/>
                        </View>
                        <Text style={styles.membersShow}>
                            Your friends who works here
                        </Text>
                        <Member />
                        <Member />
                        <Member />
                        <Member />
                        <Member />
                        <Text style={styles.membersShow}>
                            All Employees
                        </Text>
                        <Member />
                        <Member />
                        <Member />
                        <View style={styles.moreMember}>
                            <Nucleo style={{color: 'rgb(20, 122, 170)', fontSize: 14}} name='menu-dots'/>
                            {/*<Icons style={{color: 'rgb(20, 122, 170)', fontSize: 20, paddingBottom: 10}} name='arrow-down'/>*/}
                            <Icon name="angle-down" size={20} color="rgb(20, 122, 170)" />
                        </View>
                    </View>
                    <View style={styles.auctions}>
                        <CompanyHeader title="Auctions & Calls For Trends" />
                        <View style={styles.auctionContainer}>
                            <Auction />
                            <Auction />
                            <Auction />
                        </View>
                        <Text style={styles.auctionHeader}>
                            As of 2017, text messages are used by youth and adults for personal, family and social purposes and in business. Governmental
                        </Text>
                        <Text style={styles.auctionBody}>
                            As of 2017, text messages are used by youth and adults for personal, family and social purposes and in business. Governmental and non-governmental organizations use text messaging for communication between colleagues. As with emailing, in the 2010s, the sending of short informal messages has become an accepted part of many cultures.[1] This makes texting a quick and easy way to communicate with friends and colleagues, including in contexts where a call would be impolite or inappropriate (
                            {/*{this.props.substr([0, 310]) +'...'}*/}
                        </Text>
                        <View style={styles.buttonContainer}>
                            <View style={styles.details}>
                                <Icon name="bars" size={13} color="rgb(50, 152, 200)" />
                                <Text style={styles.detailsText}>Details</Text>
                            </View>
                            <View style={styles.details}>
                                <Icon name="check" size={12} color="rgb(50, 152, 200)" />
                                <Text style={styles.detailsText}>Apply</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.articles}>
                        <Text style={styles.articlesTitle}> > Posts & Articles</Text>
                        <SearchItem />
                        <SearchItem />
                        <SearchItem />
                        <SearchItem />
                        <View style={styles.moreMember}>
                            <Nucleo style={{color: 'rgb(20, 122, 170)', fontSize: 14}} name='menu-dots'/>
                            <Icon name="angle-down" size={20} color="rgb(20, 122, 170)" />
                        </View>
                    </View>
                    <View style={styles.Portfolio}>
                        <View style={styles.membersHeader}>
                            <View style={styles.left}>
                                <Text style={styles.membersTitle}> > Portfolio </Text>
                                <Text style={styles.portfolioTitleMassage}> Including Photo Galleries, Videos and related Articles </Text>
                            </View>
                            <Nucleo style={{color: 'black', fontSize: 11, paddingRight: 8, paddingBottom: 7}} name="single-01"/>
                        </View>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.portfolioSlider}>
                            <PortfolioItem likes={25} comments={58} share={10} />
                            <PortfolioItem likes={25} comments={58} share={10} />
                            <PortfolioItem likes={25} comments={58} share={10} />
                            <PortfolioItem likes={25} comments={58} share={10} />
                        </ScrollView>
                    </View>
                    <View style={styles.posts}>
                        <CompanyHeader title="Posts"/>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.PostsSlider}>
                            <PostsSliderItem />
                            <PostsSliderItem />
                            <PostsSliderItem />
                            <PostsSliderItem />
                        </ScrollView>
                    </View>
                    <View style={styles.honors}>
                        <CompanyHeader title="Honors"/>
                        <View style={{paddingTop: 20}}>
                            <SwipeableParallaxCarousel
                                style={{width: '100%'}}
                                data={slideItems}
                                height={200}
                                navigation={true}
                                navigationType="dots"
                                align="left"
                                navigationColor="rgb(20, 122, 170)"
                            />
                        </View>
                    </View>
                    <View style={styles.companies}>
                        <Text style={styles.articlesTitle}> > Related Companies </Text>
                        <RelatedCompanyItems follow={true} />
                        <RelatedCompanyItems follow={false} />
                        <RelatedCompanyItems follow={true} />
                        <View style={styles.moreMember}>
                            <Nucleo style={{color: 'rgb(20, 122, 170)', fontSize: 14}} name='menu-dots'/>
                            <Icon name="angle-down" size={20} color="rgb(20, 122, 170)" />
                        </View>
                    </View>
                    <View style={styles.career}>
                        <Text style={styles.articlesTitle}> > Fined Career Opportunities</Text>
                        <Text style={styles.articlesContent}>Check out the following companies for opportunities in your current position as:</Text>
                        <Text style={styles.careerTitle}>GUI/UX Designer</Text>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.careerSlider}>
                            <CareerItem />
                            <CareerItem />
                            <CareerItem />
                            <CareerItem />
                            <CareerItem />
                        </ScrollView>
                    </View>
                    <View style={styles.followPeople}>
                        <Text style={styles.articlesTitle}> > People you may want to follow </Text>
                        <RelatedCompanyItems follow={true} circle={true}/>
                        <RelatedCompanyItems follow={false} circle={true} />
                        <RelatedCompanyItems follow={true} circle={true} />
                    </View>
                </ScrollView>
                <Items active={0} />
            </View>
        );
    }
}
export default Companies;