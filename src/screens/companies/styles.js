import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    master: {
        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        right: 0

    },
    scroll:{
        marginTop: 46,
        marginBottom: 46,
        position: 'relative'
    },
    postType: {
        flex: 1,
        backgroundColor: 'rgb(41, 49, 52)',
        height: 35,
        flexDirection: 'row',
        position: 'absolute',
        top: 46,
        right: 0,
        left: 0,
        zIndex: 9999,
    },
    bgImage: {
        width: '100%',
        height: 410,
        opacity: .7,
        paddingTop: 54,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageContentContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 60
    },
    imageContent: {
        width: '85%',
        height: 350,
        backgroundColor: 'white',
        borderTopRightRadius: 7,
        borderTopLeftRadius: 7,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        color: 'rgb(107, 104, 104)',
        fontWeight: 'bold',
        paddingTop: 15
    },
    label: {
        fontSize: 11,
        color: 'rgb(107, 104, 104)',
        fontWeight: 'bold',
        paddingTop: 5
    },
    location: {
        fontSize: 10,
        color: 'rgb(150, 150, 150)',
        fontWeight: 'bold',
        paddingTop: 10
    },
    memberContainer: {
        flexDirection: 'row'
    },
    memberNum: {
        color: 'rgb(20, 122, 170)',
        fontSize: 11,
        fontWeight: 'bold',
        paddingRight: 5
    },
    member: {
        fontSize: 10,
        color: 'rgb(150, 150, 150)',
        fontWeight: 'bold',
    },
    followInfo: {
        fontSize: 9,
        color: 'rgb(160, 160, 160)',
        paddingTop: 15
    },
    followerContainer: {
        width: '70%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingTop: 10
    },
    followerInfo: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 20
    },
    followerName: {
        fontSize: 9,
        color: 'black',
        fontWeight: 'bold',
        paddingTop: 3,
        textAlign: 'center'
    },
    followerInfoMenu: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 30
    },
    jobs: {
        width: '33%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(230, 230, 230)',
        borderRadius: 3,
        paddingRight: 20,
        marginRight: 4
    },
    jobText: {
        color: 'rgb(20, 122, 170)',
        fontSize: 12,
        paddingLeft: 8
    },
    following: {
        width: '33%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(21, 138, 191)',
        borderRadius: 3,
        marginLeft: 4

    },
    followingText: {
        color: 'white',
        fontSize: 12
    },
    companyProfile: {
        padding: 10,
        marginTop: 30
    },
    companyProfileHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    companyProfileTitle: {
        color: 'black',
        fontSize: 12,
        fontWeight: 'bold'
    },
    companyProfileBody: {
        fontSize: 12,
        paddingTop: 20,
        lineHeight: 18
    },
    members: {
        padding: 10,
        marginTop: 30,
        paddingLeft: 10
    },
    membersHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    left: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: 10
    },
    membersTitle: {
        color: 'black',
        fontSize: 12,
        fontWeight: 'bold'
    },
    membersTitleMassage: {
        fontSize: 11,
        fontStyle: 'italic',
        color: 'rgb(50, 152, 200)',
        paddingLeft: 5
    },
    membersShow: {
        color: 'rgb(10, 112, 160)',
        fontSize: 15,
        fontWeight: 'bold',
        paddingTop: 10,
        paddingBottom: 10
    },
    moreMember: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    auctions: {
        padding: 10,
        marginTop: 30
    },
    auctionHeader: {
        fontSize: 16,
        color: 'rgb(117, 114, 114)',
        fontWeight: 'bold',
        paddingTop: 5,
        lineHeight: 20
    },
    auctionBody: {
        fontSize: 12,
        paddingTop: 20,
        lineHeight: 18
    },
    auctionContainer: {
        padding: 10
    },
    details: {
        width: '30%',
        flexDirection:'row',
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
        backgroundColor: 'rgb(245, 245, 245)',
        borderRadius: 3,
        marginRight: 10,

    },
    detailsText: {
        color: 'rgb(50, 152, 200)',
        fontSize: 12,
        paddingLeft: 8
    },
    articles: {
        padding: 10,
        paddingRight: 20
    },
    articlesTitle: {
        color: 'black',
        fontSize: 12,
        fontWeight: 'bold',
        paddingBottom: 12
    },
    articlesContent: {
        color: 'black',
        fontSize: 12,
        paddingBottom: 12
    },
    Portfolio: {
        padding: 10,

    },
    portfolioTitleMassage: {
        fontSize: 9,
        color: 'black',
        paddingLeft: 5
    },
    portfolioSlider: {
        paddingTop: 25
    },
    PostsSlider: {
        paddingTop: 10,
        marginLeft: 20,
    },
    posts: {
        padding: 10,
        paddingBottom: 20
    },
    honors: {
        padding: 15,
        paddingBottom: 20
    },
    companies: {
        padding: 10
    },
    career: {
        padding: 10,
        paddingBottom: 20
    },
    careerTitle: {
        fontSize: 15,
        color: 'black',
        fontWeight: 'bold'
    },
    careerSlider: {
        paddingTop: 10,
        marginLeft: 20,
    },
    followPeople: {
        padding: 10
    }
});

