import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        right: 0
    },
    position: {
        width: '30%',
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: 'rgb(27, 32, 35)',
        height: 40
    },
    text: {
        color: 'gray',
        paddingRight: 10

    },
    welcome: {
        height: 50,
        width: '100%'
    },
    master: {

        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    scroll:{
        padding: 15,
        marginTop: 112,
        marginBottom: 46
    },
    navHeader: {
        height: 30,
        flexDirection: 'row',
    },
    horizontalScroll: {
        flex: 1,
        position: 'absolute',
        zIndex: 9999,
        top: 82,
        left: 0,
        right: 0
    }
});

