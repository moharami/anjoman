import React, {Component} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import styles from './styles'
import SearchItem from '../../components/searchItem'
import AppTextInput from '../../components/appTextInput'
import bg from '../../assets/Logo.png';
import Ripple from 'react-native-material-ripple'
import {Actions} from 'react-native-router-flux'
import Items from '../../components/footerItems'
class Search extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            active: true,
            articleActive: true,
            connectActive: false,
            depActive: false,
            jobActive: false,
            compActive: false
        };
    }
    activeStyle(num){
        switch (num){
            case 0: {
                this.setState({articleActive: this.state.active, connectActive: !this.state.active, depActive: !this.state.active, jobActive: !this.state.active, compActive: !this.state.active });
                // Actions.push('FriendTabHome');
            }
                break;
            case 1: {
                this.setState({articleActive: !this.state.active, connectActive: this.state.active, depActive: !this.state.active, jobActive: !this.state.active, compActive: !this.state.active});
                // Actions.push('FriendTabHome');
            }
                break;
            case 2:
                this.setState({articleActive: !this.state.active, connectActive: !this.state.active, depActive: this.state.active, jobActive: !this.state.active, compActive: !this.state.active});
                // Actions.push('FriendTabHome');
                break;
            case 3:
                this.setState({articleActive: !this.state.active, connectActive: !this.state.active, depActive: !this.state.active, jobActive: this.state.active, compActive: !this.state.active});
                // Actions.push('FriendTabHome');
                break;
            case 4:
                this.setState({articleActive: !this.state.active, connectActive: !this.state.active, depActive: !this.state.active, jobActive: !this.state.active, compActive: this.state.active});
                // Actions.push('FriendTabHome');
                break;
        }
    }
    render() {
        return (
            <View style={styles.master}>
                    <View style={styles.header}>
                            <Text style={{color: 'white', fontSize: 18, paddingLeft: 20, fontWeight: 'bold'}}>New Post</Text>
                            <Image style={{width: 90, height: 20, resizeMode: 'contain', marginRight: 10}} source={bg} />
                    </View>
                    <AppTextInput />
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.horizontalScroll} >
                            <View style={styles.navHeader}>
                                <Ripple onPress={()=> this.activeStyle(0)} style={{
                                    height: 30,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: this.state.articleActive? 'rgb(20, 122, 170)': 'rgb(41, 49, 52)',
                                    paddingRight: 20,
                                    paddingLeft: 20,
                                    paddingTop: 6,
                                    paddingBottom: 6}}>
                                        <Text style={{color: this.state.articleActive?'white' :'rgb(75, 80, 84)', fontSize: 12, fontWeight: 'bold'}}>Articles</Text>
                                </Ripple>
                                <Ripple onPress={()=> this.activeStyle(1)} style={{
                                    height: 30,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: this.state.connectActive? 'rgb(20, 122, 170)': 'rgb(41, 49, 52)',
                                    paddingRight: 20,
                                    paddingLeft: 20,
                                    paddingTop: 6,
                                    paddingBottom: 6,}}>
                                        <Text style={{color: this.state.connectActive?'white' :'rgb(75, 80, 84)', fontSize: 12, fontWeight: 'bold'}}>Connections</Text>
                                </Ripple>
                                <Ripple onPress={()=> this.activeStyle(2)} style={{
                                    height: 30,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: this.state.depActive? 'rgb(20, 122, 170)': 'rgb(41, 49, 52)',
                                    paddingRight: 20,
                                    paddingLeft: 20,
                                    paddingTop: 6,
                                    paddingBottom: 6,}}>
                                        <Text style={{color: this.state.depActive?'white' :'rgb(75, 80, 84)', fontSize: 12, fontWeight: 'bold' }}>Departments</Text>
                                </Ripple>
                                <Ripple onPress={()=> this.activeStyle(3)} style={{
                                    height: 30,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: this.state.jobActive? 'rgb(20, 122, 170)': 'rgb(41, 49, 52)',
                                    paddingRight: 20,
                                    paddingLeft: 20,
                                    paddingTop: 6,
                                    paddingBottom: 6,}}>
                                        <Text style={{color: this.state.jobActive?'white' :'rgb(75, 80, 84)', fontSize: 12, fontWeight: 'bold' }}>Job Offers</Text>
                                </Ripple>
                                <Ripple onPress={()=> this.activeStyle(4)} style={{
                                    height: 30,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: this.state.compActive? 'rgb(20, 122, 170)': 'rgb(41, 49, 52)',
                                    paddingRight: 20,
                                    paddingLeft: 20,
                                    paddingTop: 6,
                                    paddingBottom: 6,
                                }}>
                                    <Text style={{color: this.state.compActive?'white' :'rgb(75, 80, 84)', fontSize: 12, fontWeight: 'bold'}}>Companies</Text>
                                </Ripple>
                            </View>
                    </ScrollView>
                <ScrollView style={styles.scroll}>
                    <SearchItem/>
                    <SearchItem/>
                    <SearchItem/>
                </ScrollView>
                <Items active={0} />
            </View>
        );
    }
}
export default Search;
