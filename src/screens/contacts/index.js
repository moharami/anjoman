import React, {Component} from 'react';
import {ScrollView, TextInput, View,Text, Image} from 'react-native';
import Contact from '../../components/contact'
import Nucleo from '../../components/nucleo'
import AppTextInput from '../../components/appTextInput'
import SearchModal from './searchModal'
import Ripple from 'react-native-material-ripple';
import Items from '../../components/footerItems'
import styles from './styles'
import bg from '../../assets/Logo.png';
import HeaderLabel from "../../components/headerLabel/index";
import HeaderRightItem from '../../components/headerRightItem'

class Contacts extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            modalVisible: false,
        };
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    render() {
        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Image style={{width: 80, height: 20, flex: 1, resizeMode: 'contain', marginRight: 15}} source={bg} />
                    <HeaderLabel  label="Connections"  notification={false} />
                    <HeaderRightItem />
                </View>
                <AppTextInput/>
                <View style={styles.label}>
                    <View style={styles.left}>
                        <Text style={styles.leftLabel}>Sort Connections by</Text>
                        <Text style={styles.rightLabel}>Recently Added</Text>
                    </View>
                    <Ripple style={styles.right} onPress={
                        () => this.setState({
                            modalVisible: !this.state.modalVisible
                        })
                    }>
                        <Nucleo style={{color: 'rgb(52, 117, 183)', fontSize: 12}} name="menu-right"/>
                    </Ripple>
                </View>
                <ScrollView style={styles.scroll}>
                    <Contact />
                    <Contact />
                    <Contact />
                    <Contact />
                    <Contact />
                    <Contact />
                    <Contact />
                    <Contact />
                    <Contact />
                    <Contact />
                </ScrollView>
                <Items active={4}/>
                <SearchModal
                    modalVisible={this.state.modalVisible}
                    onChange={(visible) => this.setModalVisible(visible)}
                />
            </View>
        );
    }
}
export default Contacts;