import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    label: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "flex-start",
        padding: 8,
        backgroundColor: 'rgb(232, 232, 232)',
        position: 'absolute',
        zIndex: 9999,
        top: 82,
        left: 0,
        right: 0
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
        width: '80%'
    },
    rightLabel: {
        fontSize: 11,
        color: 'black'
    },
    leftLabel: {
        fontSize: 12,
        color: 'rgb(52, 117, 183)',
        paddingRight: 10,
    },
    master: {
        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        right: 0

    },
    scroll:{
        marginTop: 116,
        marginBottom: 46
    },
    postType: {
        flex: 1,
        backgroundColor: 'rgb(41, 49, 52)',
        height: 35,
        flexDirection: 'row',
        position: 'absolute',
        top: 46,
        right: 0,
        left: 0,
        zIndex: 9999,

    },
});

