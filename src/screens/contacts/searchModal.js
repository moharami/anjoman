import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    Picker
}
    from 'react-native'
import Ripple from 'react-native-material-ripple';
import Toast, {DURATION} from 'react-native-easy-toast'

class searchModal extends Component {
    state = {
        modalVisible: false,
        value: ''
    }
    toggleModal(visible) {
        this.setState({ modalVisible: visible });
    }
    handleSearch(){
        if(this.state.value === '')
            this.toast.show('place select from list',1000);

    }
    render() {
        const items = [
            { key: 0, label: 'Fruits', value:'some value' },
            { key: 1, label: 'Fruits', value:{this: "could", be:"anything"} },
        ];
        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <Toast
                                ref={(ref) => this.toast = ref}
                                style={{backgroundColor:'rgb(253, 99, 61)', width: '90%', borderRadius: 5, paddingRight: 20,  }}
                                position='top'
                                positionValue={10}
                                fadeInDuration={1000}
                                fadeOutDuration={2000}
                                textStyle={{color:'white', lineHeight: 20}}
                            />
                            <Picker
                                mode="dropdown"
                                style={styles.picker}
                                selectedValue={this.state.value}
                                onValueChange={itemValue => this.setState({ value: itemValue })}>
                                    <Picker.Item  label="salary" value="value1" />
                                    <Picker.Item  label="age" value="value2" />
                            </Picker>
                            <Ripple style={styles.searchButton} onPress={()=>this.handleSearch()}>
                                <Text style={styles.searchText}>search</Text>
                            </Ripple>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default searchModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 30,
        paddingLeft: 30,

    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        height: 120,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '100%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    }
})