import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    master: {
        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        right: 0

    },
    scroll:{
        marginTop: 81,
        marginBottom: 46
    },
    postType: {
        flex: 1,
        backgroundColor: 'rgb(41, 49, 52)',
        height: 35,
        flexDirection: 'row',
        position: 'absolute',
        top: 46,
        right: 0,
        left: 0,
        zIndex: 9999,

    },
});
