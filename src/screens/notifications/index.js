import React, {Component} from 'react';
import {Text, View, ScrollView, Image, BackHandler} from 'react-native';
import SwipeableNotification from '../../components/swipeableNotification'
import Items from '../../components/footerItems'
import styles from './styles'
import Ripple from 'react-native-material-ripple'
import bg from '../../assets/Logo.png';
import {Actions} from 'react-native-router-flux'

class Notifications extends Component {
    static navigationOptions = ({ navigation }) => ({
        tabBarLabel: 'Friends Notifications',
    });
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            active: '',
            friTab: true,
            depTab: false
        };
    }
    activeStyle(tabNumber){
        if(tabNumber === 0){
            const state = this.state.friTab;
            this.setState({friTab: !state, depTab: state});
        }
        else{
            const state = this.state.depTab;
            this.setState({depTab: !state, friTab: state});
        }
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        // Actions.pop({ key: 'scene' })
        Actions.pop({ popNum: 1 })
        // Actions.pop();
    };
    render() {
        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Text style={{color: 'white', fontSize: 18, paddingLeft: 20, fontWeight: 'bold'}}>New Post</Text>
                    <Image style={{width: 90, height: 20, resizeMode: 'contain', marginRight: 10}} source={bg} />
                </View>
                <View style={styles.postType}>
                    <Ripple style={{width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.friTab? 'rgb(27, 32, 35)': 'rgb(41, 49, 52)' }} onPress={()=> this.activeStyle(0)}>
                        <Text style={{color: this.state.friTab? 'rgb(19, 122, 167)': 'rgb(75, 80, 84)', fontSize: 12}}>Friends Notifications</Text>
                    </Ripple>
                    <Ripple style={{width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.depTab? 'rgb(27, 32, 35)' :'rgb(41, 49, 52)'}} onPress={()=> this.activeStyle(1)}>
                        <Text style={{color: this.state.depTab? 'rgb(19, 122, 167)' : 'rgb(75, 80, 84)', fontSize: 12}}>Departments Notifications</Text>
                    </Ripple>
                </View>
                <ScrollView style={styles.scroll}>
                    <SwipeableNotification />
                    <SwipeableNotification />
                    <SwipeableNotification />
                </ScrollView>
                <Items active={2}/>
            </View>
        );
    }
}
export default Notifications;