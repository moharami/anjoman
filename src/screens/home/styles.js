import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
    },
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    scrollView: {
        marginTop: 92,
        marginBottom: 46
    },

    headerLabel: {
        marginLeft: 20
    },
    postType: {
        flex: 1,
        backgroundColor: 'rgb(41, 49, 52)',
        height: 35,
        flexDirection: 'row',
        position: 'absolute',
        top: 46,
        right: 0,
        left: 0,
        zIndex: 9999,

    }
});