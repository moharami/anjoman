import React, {Component} from 'react';
import {ScrollView, View, AsyncStorage, Image, Text} from 'react-native';
import Post from '../../components/post'
import Axios from 'axios'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Loader from '../../components/loader'
import Items from '../../components/footerItems'
import styles from './styles'
import ComposeIcon from '../../components/composeIcon'
import bg from '../../assets/Logo.png';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux'
import HeaderRightItem from '../../components/headerRightItem'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export const url = 'http://professionall.azarinpro.info/api';
Axios.defaults.baseURL = url;

class FriendTabHome extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            active: '',
            friTab: true,
            depTab: false
        };
    }
    async getPost(token) {
        try {
            if (this.props.posts.friendsPosts)
                this.setState({posts: this.props.posts})
            else {
            this.setState({loading: true});
            let value = await AsyncStorage.getItem(token).then((info) => {
                const newInfo = JSON.parse(info);
                if (newInfo.token !== null) {
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                    Axios.get('/feed').then(response => {
                        console.log(response);
                        this.setState({loading: false, posts: response });
                        store.dispatch({type: 'USER_POSTS_FETCHED', payload: response.data});
                    })
                    .catch(function (error) {
                            console.log(error.response);
                        }
                    );
                }
            });
        }
        } catch (error) {
            console.log(error)
        }
    }
    // componentWillMount(){
    //     this.getPost('token');
    // }
    activeStyle(tabNumber){
        if(tabNumber === 0){
            const state = this.state.friTab;
            this.setState({friTab: !state, depTab: state});
        }
        else{
            const state = this.state.depTab;
            this.setState({depTab: !state, friTab: state});
        }
    }
    render() {
        const {posts} = this.props;
        const friendsPosts = this.props.posts.friendsPosts? this.props.posts.friendsPosts: this.state.friendsPosts ;
        const departmentPosts =  this.props.posts.departmentPosts !== [] ? this.props.posts.departmentPosts : this.state.departmentPosts;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <ComposeIcon color="white" />
                    <Image style={{width: 90, height: 20, flex: 1, resizeMode: 'contain', marginLeft: '20%'}} source={bg} />
                    <Ripple onPress={()=> Actions.push('Companies')}><Text>companies</Text></Ripple>
                    <HeaderRightItem />
                </View>
                <View style={styles.postType}>
                    <Ripple style={{width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.friTab? 'rgb(27, 32, 35)': 'rgb(41, 49, 52)' }} onPress={()=> this.activeStyle(0)}>
                        <Text style={{color: this.state.friTab? 'rgb(19, 122, 167)': 'rgb(75, 80, 84)' }}>Friends Posts</Text>
                    </Ripple>
                    <Ripple style={{width: '50%', alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.depTab? 'rgb(27, 32, 35)' :'rgb(41, 49, 52)'}} onPress={()=> this.activeStyle(1)}>
                        <Text style={{color: this.state.depTab? 'rgb(19, 122, 167)' : 'rgb(75, 80, 84)'}}>Departments Posts</Text>
                    </Ripple>
                </View>
                <ScrollView style={styles.scrollView}>
                {
                    this.state.loading ? <Loader send={false} /> : (
                <View>
                    {/*{*/}
                        {/*this.state.friTab && friendsPosts && friendsPosts.map((item, index) => <Post key={index} item={item} attachments={item.attachments} />)*/}
                    {/*}*/}
                    {/*{*/}
                        {/*this.state.depTab && departmentPosts && departmentPosts.map((item, index) => <Post key={index} item={item} attachments={item.attachments} />)*/}
                    {/*}*/}
                </View>
                    )
                }
            </ScrollView>
                <Items active={1} />
            </View>

        );
    }
}
function mapStateToProps(state) {
    return {
        posts: state.posts.posts,
    }
}
export default connect(mapStateToProps)(FriendTabHome);
