import React, {Component} from 'react';
import {Text, View, ScrollView, Image, TextInput} from 'react-native';
import Nucleo from '../../components/nucleo'
import AppTextInput from '../../components/appTextInput'
import Ripple from 'react-native-material-ripple';
import JobOffer from "../../components/jobOffer/index";
import SearchModal from './searchModal'
import Items from '../../components/footerItems'
import styles from './styles'
import bg from '../../assets/Logo.png';
import HeaderLabel from "../../components/headerLabel/index";
import HeaderRightItem from '../../components/headerRightItem'

class JobOffers extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            modalVisible: false,
        };
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    render() {
        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Image style={{width: 80, height: 20, flex: 1, resizeMode: 'contain', marginRight: 15}} source={bg} />
                    <HeaderLabel  label="Job Offers"  notification={false} />
                    <HeaderRightItem />
                </View>
                <View style={styles.label}>
                    <TextInput
                        placeholder="search..."
                        placeholderTextColor="#158abf"
                        underlineColorAndroid='transparent'
                        value={this.state.text}
                        style={{
                            flex: 1,
                            height: 36,
                            backgroundColor: 'rgb(27, 32, 35)',
                            color: '#158abf',
                            paddingLeft: 15,
                            width: '100%',
                            }}
                        onChangeText={(text) => this.setState({text})} />
                    <View style={styles.position}>
                        <Text style={styles.text} >Positions</Text>
                        <Ripple style={styles.search} onPress={
                            () => this.setState({
                                modalVisible: !this.state.modalVisible
                            })
                        }>
                            <Nucleo style={{color: 'rgb(52, 117, 183)', fontSize: 16, paddingRight: 10}} name="menu-right"/>
                        </Ripple>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <JobOffer />
                    <JobOffer />
                    <JobOffer />
                    <JobOffer />
                </ScrollView>
                <Items active={5}/>
                <SearchModal
                    modalVisible={this.state.modalVisible}
                    onChange={(visible) => this.setModalVisible(visible)}
                />
            </View>
        );
    }
}
export default JobOffers