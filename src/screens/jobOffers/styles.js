import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    label: {
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: 'rgb(27, 32, 35)',
        position: 'absolute',
        zIndex: 9999,
        top: 46,
        left: 0,
        right: 0
    },
    position: {
        width: '30%',
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: "center",
        backgroundColor: 'rgb(27, 32, 35)',
       height: 40
    },
    text: {
        color: 'gray',
        paddingRight: 10

    },
    master: {
        flex: 1,
        position: 'relative',
        zIndex: 1
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(20, 122, 170)',
        height: 46,
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        right: 0

    },
    scroll:{
        marginTop: 81,
        marginBottom: 46
    },
    postType: {
        flex: 1,
        backgroundColor: 'rgb(41, 49, 52)',
        height: 35,
        flexDirection: 'row',
        position: 'absolute',
        top: 46,
        right: 0,
        left: 0,
        zIndex: 9999,

    },
});
