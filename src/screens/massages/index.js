import React, {Component} from 'react';
import {Text, View, ScrollView, TextInput, Image, BackHandler} from 'react-native';
import Massage from '../../components/massage'
import Items from '../../components/footerItems'
import styles from './styles'
import bg from '../../assets/Logo.png';
import AppTextInput from '../../components/appTextInput'
import HeaderLabel from "../../components/headerLabel/index";
import HeaderRightItem from '../../components/headerRightItem'
import {Actions} from 'react-native-router-flux'

class Massages extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    }
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        // Actions.pop();
        Actions.pop({ popNum: 1 })
    };
    render() {
        return (
            <View style={styles.master}>
                <View style={styles.header}>
                    <Image style={{width: 80, height: 20, flex: 1, resizeMode: 'contain', marginRight: 15}} source={bg} />
                    <HeaderLabel  label="Massages"  notification={true} />
                    <HeaderRightItem />
                </View>
                <AppTextInput/>
                <ScrollView style={styles.scroll}>
                    <Massage />
                    <Massage />
                    <Massage />
                    <Massage />
                    <Massage />
                </ScrollView>
                <Items active={3}/>
            </View>
        );
    }
}
export default Massages;