
const INITIAL_STATE = {
    posts: [],
    search: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_POSTS_FETCHED':
            return { ...state, posts: action.payload };
        case 'SEARCH_PAGE_ENABLED':
            return { ...state, search: true };
            case 'SEARCH_PAGE_REMOVED':
            return { ...state, search: false };
        default:
            return state;
    }
};
