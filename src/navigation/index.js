import React, {Component} from 'react';
import {Router, Stack, Scene, Drawer, Actions} from 'react-native-router-flux'
import {View, Text, Image, AsyncStorage} from 'react-native'
import FriendTabHome from '../screens/home';
import Massages from '../screens/massages';
import Contacts from '../screens/contacts';
import JobOffers from '../screens/jobOffers'
import Companies from '../screens/companies'
import Notifications from '../screens/notifications'
import Profile from '../screens/profile'
import HeaderRightItem from '../components/headerRightItem'
import HeaderLabel from '../components/headerLabel'
import AppTextInput from '../components/appTextInput'
import Item from '../components/footerNavigationItem'
import Search from '../screens/search'
import bg from '../assets/Logo.png';
import Sidebar from '../sidebar'
import ComposeIcon from '../components/composeIcon'
import ArticlePost from '../components/articlePost'
import PhotoPost from '../components/photoPost'
import VideoPost from '../components/videoPost'
import Signup from '../screens/signup'
import Login from '../screens/login'
import {store} from '../config/store';


class MainDrawerNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <Router sceneStyle={{backgroundColor: '#fff'}}>
                <Stack key="root">
                    <Scene key="drawer" drawer contentComponent={Sidebar} drawerPosition="right" hideNavBar>
                        <Scene key="Companies" initial={true} component={Login} title="Login" hideNavBar/>
                        <Scene key="Signup" component={Signup} title="Signup" hideNavBar/>
                        <Scene key="FriendTabHome" component={FriendTabHome} title="FriendTabHome" hideNavBar store={store}/>
                        <Scene key="ArticlePost" component={ArticlePost} title="ArticlePost" hideNavBar store={store} />
                        <Scene key="PhotoPost" component={PhotoPost} title="PhotoPost" hideNavBar store={store} />
                        <Scene key="VideoPost" component={VideoPost} title="VideoPost" hideNavBar store={store} />
                        <Scene key="Search" component={Search} title="Search" hideNavBar store={store} />
                        <Scene key="Notifications" component={Notifications} title="Notifications" hideNavBar store={store} />
                        <Scene key="Massages" component={Massages} title="Massages" hideNavBar store={store} />
                        <Scene key="Contacts" component={Contacts} title="Contacts" hideNavBar store={store} />
                        <Scene key="JobOffers" component={JobOffers} title="JobOffers" hideNavBar store={store} />
                        <Scene key="Profile" component={Profile} title="Profile" hideNavBar store={store} />
                        <Scene key="Companies" component={Companies} title="Companies" hideNavBar store={store} />
                    </Scene>
                </Stack>
            </Router>
        );
    }
}
export default MainDrawerNavigator;



