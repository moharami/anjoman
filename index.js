import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/app';
import {Provider} from 'react-redux';
import {store} from './src/config/store';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const Application = () => {
    return (
        <Provider store={store}>
            <App/>
        </Provider>
    );
};
AppRegistry.registerComponent('Anjoman', () => App);


